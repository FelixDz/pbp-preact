# TODO

### Current dev

#### Structure / code
- Server : switch to something faster than Express. Maybe [Polka](https://github.com/lukeed/polka) or some Go server ?
- Rich Snippets : reviews, local business, organization
- API : voyager config -> add cascade delete on media picker
- API : change file renaming when using media picker, see [Media Picker docs](https://voyager-docs.devdojo.com/bread/introduction-1/media-picker#watermark)

#### Known API bugs

#### Mailing
- [listmonk](https://listmonk.app/)
- [mautic](https://www.mautic.org/)
- See [listmonk docs](https://listmonk.app/docs/configuration/)

***

### Deploy

#### To Be Done
- Legal mentions (see free tool saved in favs)
- sitemap.xml
- Check all `Design / Site` emails
- Change nav images according to section titles
- Correct metas :
    + [https://www.foulquier.info/tutoriaux/mise-en-place-meta-og-facebook-dans-une-page-html]
    + Update small image : appealing one instead of logo
    + apple-touch-icon: For ideal appearance on iOS when users add a progressive web app to the home screen, define an `apple-touch-icon`. It must point to a non-transparent 192px (or 180px) square PNG
    + Maskable icon : https://web.dev/maskable-icon-audit
- Build all useful caches in Laravel app

#### How to
- See [this link](https://faq.o2switch.fr/hebergement-mutualise/tutoriels-cpanel/app-nodejs#deploiement-d-une-application-generique)

***

### Future devs

- Calendar ? (for fast review of upcoming (or past) events)
- Recipes (with photos, quantities / person)
- HTTP Caching
- [Photos] : On albums, show a "Share button" that links to a page showing a specific image.
- [Optimization] shouldComponentUpdate for performance
- [Mobile optimization] : Load only used data (stylesheets / components / images)
- [Products] : which beer do you like ? See [la Soyeuse](https://www.lasoyeuse.fr/quelle-soyeuse-etes-vous.html)
- [Fonts] : preload, [see here](https://stackoverflow.com/questions/55397432/how-to-preload-a-css-font-face-font-that-is-bundled-by-webpack4babel), and [here](https://www.npmjs.com/package/webpack-font-preload-plugin)
- [Fonts] : compress with brotli ?

    /*
    const htmlPlugin = helpers.getPluginsByName(config, 'HtmlWebpackPlugin')[0];
    if (htmlPlugin) {
        config.plugins.push(
            new PreloadPlugin({
                rel: 'preload',
                // include: 'initial',
                as(entry) {
                    if (/\.css$/.test(entry)) return 'style';
                    if (/\.(eot|svg|ttf|woff2?)$/.test(entry)) return 'font';
                    if (/\.(png|jpe?g|gif|svg|webp)$/.test(entry)) return 'image';
                    return 'script';
                },
                fileWhitelist: [/\.(ttf|woff2?)$/],
            })
        )
    }
    */
