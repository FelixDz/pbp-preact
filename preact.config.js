const path = require('path')

export default (config, env, helpers, options) => {
	/*
	if (!env.isProd) {
		config.devServer.proxy = [
			{
				path: '/api/**',
				target: 'http://pbp-api.dev/',
				secure: true,
				changeOrigin: true
			}
		]
	}
	*/

	// Disable sourceMaps on prod
	if (env.isProd) {
		config.devtool = false;
	}

	// Don't copy the "assets" folder
	/*
	const copyPlugin = helpers.getPluginsByName(config, 'CopyWebpackPlugin')[0];
	if (copyPlugin) {
		copyPlugin.plugin.patterns = copyPlugin.plugin.patterns.filter(pattern => pattern.from == "assets");
	}
	*/

	// Inline fonts
	const critters = helpers.getPluginsByName(config, 'Critters')[0];
	if (critters) critters.plugin.options.inlineFonts = true;

	config.resolve.alias = {
		'@': path.resolve(__dirname, 'src/components/'),
		'@style': path.resolve(__dirname, 'src/style/'),
		'@assets': path.resolve(__dirname, 'src/assets/'),
		...config.resolve.alias,
	}
}
