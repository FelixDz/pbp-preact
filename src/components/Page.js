import { h, Component } from 'preact'
import { Router } from 'preact-router'

import Content from '@/global/content/Content'
import Sidebar from '@/global/sidebar/Sidebar'

import {scrollTo} from '@/utils/Utils'

// All the app "routing" is done here : the url gives the correct component(s)
import HomePage from 'async!@/pages/HomePage'
// Beers
import ProductsPage from 'async!@/pages/ProductsPage'
import EthicsPage from 'async!@/pages/EthicsPage'
import AwardsPage from 'async!@/pages/AwardsPage'
import RetailOutletsPage from 'async!@/pages/RetailOutletsPage'
// Brewery
import HistoryPage from 'async!@/pages/HistoryPage'
import BiergartenPage from 'async!@/pages/BiergartenPage'
import VisitsPage from 'async!@/pages/VisitsPage'
// Events
import EventsPage from 'async!@/pages/EventsPage'
import EventsHomePage from 'async!@/pages/EventsHomePage'
import EventsElsewherePage from 'async!@/pages/EventsElsewherePage'
// Gallery
import PhotosPage from 'async!@/pages/PhotosPage'
import VideosPage from 'async!@/pages/VideosPage'
import HumourPage from 'async!@/pages/HumourPage'
import ReviewsPage from 'async!@/pages/ReviewsPage'
// Contact
import ContactPage from 'async!@/pages/ContactPage'
import ProsPage from 'async!@/pages/ProsPage'
// Other
import NewsPage from 'async!@/pages/NewsPage'
import TestPage from 'async!@/pages/TestPage'
import MentionsPage from 'async!@/pages/MentionsPage'
// Errors
import ErrorPage from 'async!@/pages/ErrorPage'

export default class Page extends Component {

	handleRoute = () => {
		scrollTo(0, 100, 'easeOutQuad')
	}

	render(props) {
		return (
			<section className="Page">
				<Sidebar />

				<Content>
					<Router url={props.url} onChange={this.handleRoute}>

						<HomePage path="/" />

						<ProductsPage path="/produits" />
						<EthicsPage path="/ethique" />
						<AwardsPage path="/recompenses" category="award"/>
						<AwardsPage path="/recompenses/:newsId" category="award"/>
						<RetailOutletsPage path="/points-de-vente" />

						<HistoryPage path="/histoire" />
						<BiergartenPage path="/biergarten" />
						<VisitsPage path="/visites" />

						<EventsPage path="/evenements" category="event" />
						<EventsPage path="/evenements/:newsId" category="event" />
						<EventsHomePage path="/evenements-brasserie" category="event_home"/>
						<EventsHomePage path="/evenements-brasserie/:newsId" category="event_home"/>
						<EventsElsewherePage path="/evenements-ailleurs" category="event_elsewhere"/>
						<EventsElsewherePage path="/evenements-ailleurs/:newsId" category="event_elsewhere"/>

						<PhotosPage path="/photos" />
						<PhotosPage path="/album/:albumId" />
						<VideosPage path="/videos" />
						<HumourPage path="/humour" />
						<ReviewsPage path="/avis" />

						<ContactPage path="/contact" />
						<ProsPage path="/pros" />

						<NewsPage path="/nouvelles" />
						<NewsPage path="/nouvelles/:newsId" />

						<MentionsPage path="/mentions" />

						<TestPage path="/test" />

						<ErrorPage type="404" default />
					</Router>
				</Content>
			</section>
		)
	}
}
