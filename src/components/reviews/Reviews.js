import { h, Component } from 'preact'

import Fetcher from '@/global/fetcher/Fetcher'
import Review from '@/reviews/Review'

import style from './Reviews.scss'

export default class Reviews extends Component {

	renderReviews = (reviews) => {
		return (
			<div class={style.Reviews}>
				{
					reviews.slice(0, this.props.limit ? this.props.limit : reviews.length).map((review, index) => {
						return (
							<Review reviewData={review} imageAlt={index % 2 === 0} />
						)
					})
				}
			</div>
		)
	}

	render(props) {

		return (
			<Fetcher
				query={"reviews" + (props.category ? ("/" + props.category) : "")}
				renderData={this.renderReviews}
				loadingText="Chargement des avis en cours…" 
				errorText="Tout le monde s'en fout ? Non… seulement notre serveur web, apparemment ! Retrouvez des avis sur la brasserie via sa page Facebook ou TripAdvisor, et si le problème persiste, dites-le nous, merci ;)"
			/>
		)
	}
}
