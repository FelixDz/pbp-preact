import { h, Component } from 'preact'

import Image from '@/global/images/Image'
import Rating from '@/global/rating/Rating'

import style from './Review.scss'

export default class Review extends Component {

	renderReview = (review) => {

		return (
			<div class={style.Review}>

				<Image class={[style.ReviewImage, this.props.imageAlt ? style.ReviewImageAlt : ""].join(" ")} title={review.author} alt={review.author} src={review.image} databaseImage innerShadow circular />

				<div class={style.ReviewText}>

					<div class={style.ReviewDesc}><span class={style.ReviewDescQuote}></span><p>{review.desc}”</p></div>

					<div class={style.ReviewInfo}>
						<span class={style.ReviewAuthor}>{review.author}</span>
						{review.via &&
							<span class={style.ReviewVia}> <span class={style.ReviewDash}>&mdash;</span> via {review.via}</span>
						}
						{review.rating &&
							<span class={style.ReviewRating}> <span class={style.ReviewDash}>&mdash;</span> <Rating rating={review.rating} renderHalfElems /></span>
						}
					</div>
				</div>
			</div>
		)
	}

	render(props) {

		return (
			this.renderReview(props.reviewData)
		)
	}
}
