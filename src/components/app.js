import { h, Component } from 'preact'
import Helmet from 'preact-helmet'
import style from './App.scss'

import {debounce} from '@/utils/Utils'
import Nav from '@/global/nav/Nav'
import Header from '@/global/header/Header'
import Page from '@/Page'
import ScrollUp from '@/global/scrollup/ScrollUp'
import Spacer from '@/global/spacer/Spacer'
import Footer from '@/global/footer/Footer'

export default class App extends Component {

	constructor (props) {
		super(props)
		this.metas = {
			title: 'Petite Brasserie Picarde | Bières biologiques de caractère',
			url: 'https://www.petitebrasseriepicarde.fr/',
			image: '/assets/icons/LogoPBP_300x300.png',
			description:
				'La Petite Brasserie Picarde est une brasserie artisanale proposant des bières biologiques de caractères. Située à Grandfresnoy (Oise, Picardie), elle propose régulièrement des animations et visites sur place.'
		}

		this.state = {
			isUserReading: false
		}
	}

	componentDidMount () {
	    window.addEventListener('scroll', this.handleScroll)
	}

	componentWillUnmount () {
	    window.removeEventListener('scroll', this.handleScroll)
	}

	handleScroll = debounce(() => {

		if (document.getElementById('Header').getBoundingClientRect().bottom <= 0) {
			if (this.state.isUserReading == false) {
				this.setState({ isUserReading: true })
			}
		} else if (this.state.isUserReading == true) {
			this.setState({ isUserReading: false })
		}

	}, 100)

	render(props, state) {

		let appClasses = [style.App, state.isUserReading ? 'readingMode' : ''].join(' ')

		return (
			<div id="app" class={appClasses}>
				<Helmet
					defaultTitle={this.metas.title}
					titleTemplate="%s | Petite Brasserie Picarde"
					meta={[
						{ name: 'description', content: this.metas.description },
						// Facebook meta
						{ property: 'og:title', content: this.metas.title },
						{ property: 'og:type', content: 'website' },
						{ property: 'og:locale', content: 'fr_FR' },
						{ property: 'og:description', content: this.metas.description },
						{ property: 'og:url', content: this.metas.url },
						{ property: 'og:site_name', content: 'Petite Brasserie Picarde' },
						{ property: 'og:image', content: this.metas.image },
						{ property: 'og:image:secure_url', content: this.metas.image },
						// Twitter meta
						{ name: 'twitter:title', content: this.metas.title },
						{ name: 'twitter:creator', content: 'Petite Brasserie Picarde' },
						{ name: 'twitter:site', content: 'https://www.petitebrasseriepicarde.fr/' },
						{ name: 'twitter:card', content: 'summary' },
						{ name: 'twitter:image', content: this.metas.image },
						{ name: 'twitter:description', content: this.metas.description }
					]}
					link={[{ rel: 'canonical', href: this.metas.url }]}
				/>
				<Nav />
				<Header />
				<Page />
				<ScrollUp />
				<Spacer huge />
				<Footer />
			</div>
		)
	}
}
