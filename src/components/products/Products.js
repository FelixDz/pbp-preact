import { h, Component } from 'preact'
import { Link } from 'preact-router/match'

import Fetcher from '@/global/fetcher/Fetcher'
import Image from '@/global/images/Image'
import SearchBar from '@/global/searchbar/SearchBar'
import Spacer from '@/global/spacer/Spacer'
import Popup from '@/global/popup/Popup'
import { objectContainsValue } from '@/utils/Utils'
import SecondaryLink from '@/global/links/secondary/SecondaryLink'

import GlobalAward from '@/products/GlobalAward'
import Product from '@/products/Product'

import style from './Products.scss'

export default class Products extends Component {

	constructor() {
		super()
		this.state = {
			searchValue: "",
			ingredientsPopup: false
		}
	}

	dismissIngredientsPopup = () => {
		this.setState({ ingredientsPopup: false })
	}

	openIngredientsPopup = (event) => {
		this.setState({ ingredientsPopup: true })
	}	

	handleSearch = () => {

		let searchValue = document.querySelector("."+style.ProductsSearchInput).getAttribute("value")
	    this.setState({ searchValue: searchValue })
	}

	renderGlobalAwards = (products) => {

		let globalProduct = products.find((p) => { return p.category === "global"})

		return (typeof globalProduct !== 'undefined') && globalProduct.awards.map(globalAward => {
			return (
				<GlobalAward awardData={globalAward} />
			)
		})
	}

	renderProductsList = (products) => {
		return products
			.filter(p => {return p.category !== "global"})
			.filter(p => objectContainsValue(p, this.state.searchValue))
			.sort((a, b) => a.name.localeCompare(b.name))
			.map(product => {
				return (
					<Product productData={product} />
				)
			})
	}

	renderProducts = (products) => {
		return (
			<div>
				<section class={style.GlobalAwards}>
					{this.renderGlobalAwards(products)}
				</section>

				<div className={style.ProductsSearch}>
					<SearchBar onSearch={this.handleSearch} value={this.state.searchValue} searchPlaceHolder='Rechercher un produit... Ex: "blonde", "Mirac", "sarrasin"' inputClass={style.ProductsSearchInput} />
				</div>

				<section class={style.ProductsList}>
					{this.renderProductsList(products)}
				</section>
			</div>
		)
	}

	render(props) {

		return (
			<div class={style.Products}>
				<h1>Nos produits</h1>
				<p>
				<span class="bold textPrimary">Mes bières sont naturelles.</span> Je n’ajoute aucun produit pour améliorer le rendement, favoriser la tenue de mousse, clarifier, acidifier, réduire certains sels minéraux, en augmenter d’autres, conserver plus longtemps, etc., alors que ces additifs sont hélas très courants. Les seuls ingrédients utilisés sont : <span class="bold">eau, céréales, houblon, levure</span> et, pour certaines spécialités comme la bière de Noël, des <span class="italic">plantes</span>, des <span class="italic">épices</span> ou du <span class="italic">miel</span>.<br />
					<br />
					<span class="italic">Un mot sur l'AB ?</span>
				</p>

				<SecondaryLink
					href=""
					title="Plus d'infos sur les ingrédients"
					alt="Plus d'infos sur les ingrédients"
					onClick={this.openIngredientsPopup.bind(this)}
					centered
				>
					Plus d'infos sur les ingrédients
				</SecondaryLink>

				<Popup isOpen={this.state.ingredientsPopup} onDismiss={this.dismissIngredientsPopup} title="Les ingrédients de nos bières">
					<p>
						L’eau est puisée à <span class="bold">Grandfresnoy</span> à 30 m sous le sol et régulièrement contrôlée par les services sanitaires. Elle est très riche en carbonate de calcium (calcaire dû au « cran », la craie picarde), ce qui donne leur <span class="bold">caractère régional</span> à nos bières.<br /><br />
						Les autres ingrédients sont tous <span class="bold">choisis avec le plus grand soin</span> dans la meilleure qualité. Cette exigence et des méthodes de brassage et de fermentation rigoureuses sont les uniques garants de la qualité irréprochable de nos productions.
					</p>
				</Popup>

				<Spacer huge />

				<Fetcher
					query={"products"}
					renderData={this.renderProducts}
					loadingText="Chargement des produits en cours..." 
					errorText="Comment ça, pas de bière ?! Cela ne se peut ! Rassurez-vous, le souci n'est qu'informatique… Venez vérifier à la brasserie (ou bien contactez-nous) !"
				/>
			</div>
		)
	}
}
