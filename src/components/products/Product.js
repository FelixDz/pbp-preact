import { h, Component } from 'preact'
import { Link } from 'preact-router/match'

import Image from '@/global/images/Image'
import BitternessRating from '@/global/rating/BitternessRating'
import BodyRating from '@/global/rating/BodyRating'
import {slugify} from '@/utils/Utils'

import style from './Product.scss'

export default class Product extends Component {

	printCategory = (cat) => {
		switch (cat) {
			case "beer":
				return "Bière"
				break
			case "composition":
				return "Composition"
				break
			case "other":
			default:
				return "Autre"
		}
	}

	renderBeerAwards = (awards) => {

		return awards.map(award => {
			return (
				<Link class={style.BeerAwardLink} href={"/recompenses/" + award.id} title={award.award_title + " (" + award.year + ")"} alt={award.award_title + " (" + award.year + ")"} >
					<Image class={style.BeerAwardImage} title={award.award_title + " (" + award.year + ")"} alt={award.award_title + " (" + award.year + ")"} src={award.image} databaseImage smallRounded fit />
				</Link>
			)
		})
	}

	renderProduct = (product) => {

		return (
			<div class={style.Product} id={slugify(product.name, false)}>

				<Image class={style.ProductImage} title={product.name} alt={product.name} src={product.image} databaseImage rounded fullWidth />

				<div class={style.ProductTitle}>
					<h4 class={style.ProductName}>{product.name}</h4>

					<h5 class={style.ProductCategory}>
						{product.category === "beer"
							? <span><span class={style.BeerType}>{product.type}</span> &mdash; <span class={style.BeerAlcohol}>{product.abv} % <span class={style.BeerAlcoholAlcVol}>alc. vol.</span></span></span>
							: this.printCategory(product.category)
						}
					</h5>
				</div>

				<div class={style.ProductDesc}><p>{product.desc}</p></div>

				{product.category === "beer" &&

					<div class={style.BeerInfos}>
						<div class={style.BeerSpecs}>
							<div class={style.BeerSpecBody}>
								<span class={style.BeerSpecLabel}>Corps</span> <span class={style.BeerSpecData}>
									<BodyRating rating={product.body} />
								</span>
							</div>
							<div class={style.BeerSpecBitterness}>
								<span class={style.BeerSpecLabel}>Amertume</span> <span class={style.BeerSpecData}>
									<BitternessRating rating={product.bitterness} />
								</span>
							</div>
						</div>

						{product.awards.length !== 0 &&
							<div class={style.BeerAwards}>
								<div class={style.BeerAwardsLabel}>M&eacute;dailles</div>
								<div class={style.BeerAwardsList}>
									{this.renderBeerAwards(product.awards)}
								</div>
							</div>
						}

					</div>
				}
			</div>
		)
	}

	render(props) {

		return (
			this.renderProduct(props.productData)
		)
	}
}
