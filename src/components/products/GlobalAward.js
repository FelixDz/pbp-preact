import { h, Component } from 'preact'
import { Link } from 'preact-router/match'

import Image from '@/global/images/Image'

import style from './GlobalAward.scss'

export default class GlobalAward extends Component {

	renderGlobalAward = (award) => {
		return (
			<div class={style.GlobalAward}>
				<Link class={style.GlobalAwardLink} href={"/recompenses/" + award.id} title="En apprendre plus sur cette nomination" alt="En apprendre plus sur cette nomination" >
					<Image class={style.GlobalAwardImage} title="En apprendre plus sur cette nomination" alt="En apprendre plus sur cette nomination" src={award.image} databaseImage thumb rounded fit />
					<p class={style.GlobalAwardText}>{award.award_title}<br /><span class={style.GlobalAwardYear}>{award.year}</span></p>
				</Link>
			</div>
		)
	}

	render(props) {
		return (
			this.renderGlobalAward(props.awardData)
		)
	}
}
