@import '~@style/_vars';

.Product {

	border-top: 1px solid $sep-color;
	padding: $large-gap 0;
	display: grid;
	grid-template-columns: 100%;
	grid-template-rows: none;
	grid-template-areas: 
		"title"
		"image"
		"desc"
		"infos";
	gap: $small-gap $medium-gap;
	align-items: start;

	font-size: 0.9rem;

	@include respond-to(tablets) {
		padding: $medium-gap-T 0;
		grid-template-columns: 240px 1fr;
		grid-template-areas: 
			"title title"
			"image ."
			"desc desc"
			"infos .";
		gap: $small-gap-T $medium-gap-T;
	}

	@include respond-to(screens) {
		padding: $medium-gap-S 0;
		grid-template-columns: calc(30% - #{$medium-gap-S}/2) calc(70% - #{$medium-gap-S}/2);
		grid-template-areas: 
			". title"
			"image desc"
			". infos";
		gap: $small-gap-S $medium-gap-S;
	}
	
	.ProductImage {

		grid-area: image;
		align-self: center;
		position: relative;
		width: 100%; height: auto;
		@include respond-to(handhelds) { max-width: $product-specs-max-width; }
		@include respond-to(tablets-and-larger) { margin: auto; }
	}

	.ProductTitle {

		grid-area: title;

		.ProductName {
			font-family: "Berkshire Swash", sans-serif;
			font-size: 1.6rem;
			margin: 0;
			margin-left: -0.3rem;

			color: $lighter-text-color;
		}

		.ProductCategory {

			display: inline-block;
			margin: 0;

			font-family: "Source Sans Pro", sans-serif;
			font-weight: 400;
			text-transform: uppercase;
			font-size: 0.8rem;
			color: $darker-text-color;

			.BeerType {
				// font-weight: 600;
			}

			.BeerAlcohol {
				.BeerAlcoholAlcVol {
					font-size: 0.6rem;
				}
			}
		}
	}

	.ProductDesc {

		grid-area: desc;
		@include respond-to(screens) { align-self: center; }

		p {
			margin: 0;
			line-height: 1rem;
			font-size: 0.8rem;
		}
	}

	.BeerInfos {

		grid-area: infos;

		display: flex;
		flex-flow: column nowrap;
		justify-content: space-between;
		align-items: stretch;

		@include respond-to(tablets-and-larger) {
			flex-flow: row wrap;
			align-items: center;
		}

		.BeerSpecs {
			flex: 1 0 auto;
			@include respond-to(handhelds) { max-width: $product-specs-max-width; }
		}

		.BeerSpecBody, .BeerSpecBitterness, .BeerAwards {
			display: flex;
			flex-flow: row nowrap;
			justify-content: space-between;
			align-items: center;
			width: 100%;
			@include respond-to(screens) { width: auto; }
		}

		.BeerSpecLabel, .BeerAwardsLabel {
			color: $darker-text-color;
			font-size: 0.55rem;
			text-transform: uppercase;
			padding-left: $small-gap;

			display: inline-block;
			flex: 5 0 auto;
			max-width: 100px;
		}

		.BeerSpecData {
			text-align: right;
			display: inline-block;
			flex: 1 0 auto;
		}

		.BeerAwards {

			@include respond-to(handhelds) { max-width: $product-specs-max-width; }

			margin-top: $small-gap;
			@include respond-to(tablets) { margin-top: $small-gap-T; }
			@include respond-to(screens) { margin-top: $small-gap-S; }

			.BeerAwardsList {

				flex: 1 0 auto;

				display: flex;
				flex-flow: row wrap;
				justify-content: flex-end;

				.BeerAwardLink {

					.BeerAwardImage {

						width: 50px; height: 50px;
						margin-left: $small-gap;
						@include respond-to(tablets) { margin-left: $small-gap-T; }
						@include respond-to(screens) { margin-left: $small-gap-S; }
					}
				}
			}
		}
	}
}
