import { h, Component } from 'preact'
import Helmet from 'preact-helmet'
import { Link } from 'preact-router/match'

import Image from '@/global/images/Image'
import Spacer from '@/global/spacer/Spacer'
import Reviews from 'async!@/reviews/Reviews'

export default class ReviewsPage extends Component {
	constructor (props) {
		super(props)
		this.metas = {
			title: 'Avis sur la brasserie',
			url: 'https://www.petitebrasseriepicarde.fr/avis',
			image: 'https://picsum.photos/200',
			description: 'Les visiteurs de la brasserie vous donnent leur avis.'
		}
	}

	render() {
		return (
			<section class="ReviewsPage">
				<Helmet
					title={this.metas.title}
					meta={[
						{ name: 'description', content: this.metas.description },
						// Facebook meta
						{ property: 'og:title', content: this.metas.title + ' | Petite Brasserie Picarde' },
						{ property: 'og:type', content: 'article' },
						{ property: 'og:description', content: this.metas.description },
						{ property: 'og:url', content: this.metas.url },
						{ property: 'og:site_name', content: 'Petite Brasserie Picarde' },
						{ property: 'og:image', content: this.metas.image },
						{ property: 'og:image:secure_url', content: this.metas.image },
						// Twitter meta
						{ name: 'twitter:title', content: this.metas.title + ' | Petite Brasserie Picarde' },
						{ name: 'twitter:creator', content: 'Petite Brasserie Picarde' },
						{ name: 'twitter:site', content: 'https://www.petitebrasseriepicarde.fr/' },
						{ name: 'twitter:card', content: 'summary' },
						{ name: 'twitter:image', content: this.metas.image },
						{ name: 'twitter:description', content: this.metas.description }
					]}
					link={[{ rel: 'canonical', href: this.metas.url }]}
				/>
				<h1>Les avis de nos visiteurs</h1>

				<p>
					Il·elles sont venu·es à la brasserie et nous ont livré leurs impressions par <Link href="mailto:contact@petitebrasseriepicarde.fr" title="Joignez-nous par mail">mail</Link>, sur <Link href="https://www.facebook.com/petitebrasseriepicarde/" title="Retrouvez-nous sur Facebook">Facebook</Link>, <Link href="https://www.tripadvisor.fr/Attraction_Review-g4765232-d4759277-Reviews-Petite_Brasserie_Picarde-Grandfresnoy_Oise_Hauts_de_France.html" title="Retrouvez-nous sur TripAdvisor">TripAdvisor</Link>…
				</p>

				<Spacer large />

				<Reviews />
				
			</section>
		)
	}
}
