import { h, Component } from 'preact'
import Helmet from 'preact-helmet'
import { Link } from 'preact-router/match'

export default class MentionsPage extends Component {
	constructor (props) {
		super(props)
		this.metas = {
			title: 'Mentions',
			url: 'https://www.petitebrasseriepicarde.fr/mentions',
			image: 'https://picsum.photos/200',
			description: 'Mentions légales pour le site de la brasserie.'
		}
	}

	render() {
		return (
			<section class="MentionsPage">
				<Helmet
					title={this.metas.title}
					meta={[
						{ name: 'description', content: this.metas.description },
						// Facebook meta
						{ property: 'og:title', content: this.metas.title + ' | Petite Brasserie Picarde' },
						{ property: 'og:type', content: 'article' },
						{ property: 'og:description', content: this.metas.description },
						{ property: 'og:url', content: this.metas.url },
						{ property: 'og:site_name', content: 'Petite Brasserie Picarde' },
						{ property: 'og:image', content: this.metas.image },
						{ property: 'og:image:secure_url', content: this.metas.image },
						// Twitter meta
						{ name: 'twitter:title', content: this.metas.title + ' | Petite Brasserie Picarde' },
						{ name: 'twitter:creator', content: 'Petite Brasserie Picarde' },
						{ name: 'twitter:site', content: 'https://www.petitebrasseriepicarde.fr/' },
						{ name: 'twitter:card', content: 'summary' },
						{ name: 'twitter:image', content: this.metas.image },
						{ name: 'twitter:description', content: this.metas.description }
					]}
					link={[{ rel: 'canonical', href: this.metas.url }]}
				/>
				<h1>Mentions légales</h1>

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit.
					Magnam voluptatem possimus nobis autem aut sit quas dolor
					nostrum. Aliquid laudantium quo explicabo sequi cumque
					incidunt assumenda, fuga eos non inventore. <Link href="/" title="">Retour à l'accueil</Link>.
				</p>
				
			</section>
		)
	}
}
