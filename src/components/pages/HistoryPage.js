import { h, Component } from 'preact'
import Helmet from 'preact-helmet'
import { Link } from 'preact-router/match'

import Image from '@/global/images/Image'

export default class HistoryPage extends Component {
	constructor (props) {
		super(props)
		this.metas = {
			title: 'Histoire de la brasserie',
			url: 'https://www.petitebrasseriepicarde.fr/histoire',
			image: 'https://picsum.photos/200',
			description: 'Découvrez l\'histoire de la Petite Brasserie Picarde : de l\'enfance du futur brasseur à l\'installation de la brasserie, et toujours de nouveaux projets !'
		}
	}

	render() {
		return (
			<section class="HistoryPage">
				<Helmet
					title={this.metas.title}
					meta={[
						{ name: 'description', content: this.metas.description },
						// Facebook meta
						{ property: 'og:title', content: this.metas.title + ' | Petite Brasserie Picarde' },
						{ property: 'og:type', content: 'article' },
						{ property: 'og:description', content: this.metas.description },
						{ property: 'og:url', content: this.metas.url },
						{ property: 'og:site_name', content: 'Petite Brasserie Picarde' },
						{ property: 'og:image', content: this.metas.image },
						{ property: 'og:image:secure_url', content: this.metas.image },
						// Twitter meta
						{ name: 'twitter:title', content: this.metas.title + ' | Petite Brasserie Picarde' },
						{ name: 'twitter:creator', content: 'Petite Brasserie Picarde' },
						{ name: 'twitter:site', content: 'https://www.petitebrasseriepicarde.fr/' },
						{ name: 'twitter:card', content: 'summary' },
						{ name: 'twitter:image', content: this.metas.image },
						{ name: 'twitter:description', content: this.metas.description }
					]}
					link={[{ rel: 'canonical', href: this.metas.url }]}
				/>
				<h1>L'histoire du lieu… et du brasseur</h1>

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit.
					Magnam voluptatem possimus nobis autem aut sit quas dolor
					nostrum. Aliquid laudantium quo explicabo sequi cumque
					incidunt assumenda, fuga eos non inventore. <Link href="/" title="">Back to home</Link>.
				</p>
				
			</section>
		)
	}
}
