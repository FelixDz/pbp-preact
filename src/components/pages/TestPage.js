import { h, Component } from 'preact'
import Helmet from 'preact-helmet'
import { Link } from 'preact-router/match'

import style from './Test.scss'

import Image from '@/global/images/Image'
import SecondaryLink from '@/global/links/secondary/SecondaryLink'
import Popup from '@/global/popup/Popup'
import Spacer from '@/global/spacer/Spacer'

export default class TestPage extends Component {

	state = {
		subscriptionPopup: false
	}

	constructor (props) {
		super(props)
		this.metas = {
			title: 'Page de test',
			url: 'https://www.petitebrasseriepicarde.fr/test',
			image: 'https://picsum.photos/200',
			description: 'Ceci est une page de test.'
		}
	}

	componentDidMount () {
	}

	dismissSubscriptionPopup = () => {
		this.setState({ subscriptionPopup: false })
	}

	openSubscriptionPopup = () => {
		this.setState({ subscriptionPopup: true })
	}

	render(state) {
		return (
			<section class="TestPage">
				<Helmet
					title={this.metas.title}
					meta={[
						{ name: 'description', content: this.metas.description },
						// Facebook meta
						{ property: 'og:title', content: this.metas.title + ' | Petite Brasserie Picarde' },
						{ property: 'og:type', content: 'article' },
						{ property: 'og:description', content: this.metas.description },
						{ property: 'og:url', content: this.metas.url },
						{ property: 'og:site_name', content: 'Petite Brasserie Picarde' },
						{ property: 'og:image', content: this.metas.image },
						{ property: 'og:image:secure_url', content: this.metas.image },
						// Twitter meta
						{ name: 'twitter:title', content: this.metas.title + ' | Petite Brasserie Picarde' },
						{ name: 'twitter:creator', content: 'Petite Brasserie Picarde' },
						{ name: 'twitter:site', content: 'https://www.petitebrasseriepicarde.fr/' },
						{ name: 'twitter:card', content: 'summary' },
						{ name: 'twitter:image', content: this.metas.image },
						{ name: 'twitter:description', content: this.metas.description }
					]}
					link={[{ rel: 'canonical', href: this.metas.url }]}
				/>
				<h1>Page de test</h1>

				Fen&ecirc;tre modale : <br />

				<SecondaryLink
					href=""
					title="Ouvrir une fenêtre modale"
					alt="Ouvrir une fenêtre modale"
					onClick={this.openSubscriptionPopup}
					centered
				>
					Ouvrir une fen&ecirc;tre modale
				</SecondaryLink>


				<Spacer huge />

				<h2>Form</h2>

				<form method="post" action="http://listmonk.test:9000/subscription/form" class="listmonk-form">
					<div>
						<h3>Inscription à la newsletter</h3>

						<p><input type="text" name="email" placeholder="E-mail" /></p>
						<p><input type="text" name="name" placeholder="Nom" /></p>
						<p><input type="text" name="zipcode" placeholder="Code postal" /></p>
						<input id="505c3" type="hidden" name="l" value="505c3203-3db5-4879-a2de-71e1e34e1460" />
						<p><input type="submit" value="Inscription" /></p>
					</div>
				</form>

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit.
					Magnam voluptatem possimus nobis autem aut sit quas dolor
					nostrum. Aliquid laudantium quo explicabo sequi cumque
					incidunt assumenda, fuga eos non inventore. <Link href="/">Back to home</Link>.
				</p>

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit.
					Ipsum laborum voluptates, impedit non. Impedit, similique
					expedita sapiente exercitationem alias ut ad, officia rem
					sequi illum tempora quas, perferendis id nesciunt.
				</p>
				<p>
					Itaque aliquam ullam perferendis quas distinctio inventore
					atque iusto et. Aperiam soluta eaque dolor provident ex amet
					dolore, eos deserunt officia unde pariatur quasi culpa
					minima repellat, quod veniam, laudantium.
				</p>
				<p>
					Optio architecto rerum alias consequuntur, beatae,
					voluptatum quod dolorem laboriosam tempora delectus tenetur
					ex soluta magnam iure, praesentium doloribus exercitationem
					nihil cupiditate laudantium. Ducimus deleniti repellat ad
					voluptatem fugit eaque?
				</p>
				<p>
					Fuga cumque veniam, molestiae tenetur veritatis minima
					deleniti corporis dolore vero, impedit voluptatibus
					repellendus esse recusandae facilis numquam sequi laborum
					eveniet, nulla! Corporis dolores ipsa commodi officia nulla
					tenetur doloremque.
				</p>
				<p>
					Ad facilis, error, adipisci optio enim minima id. Pariatur
					et praesentium fugiat esse nisi, modi dolorum odio nobis,
					ducimus vero cum. In ducimus veritatis est distinctio sunt,
					omnis voluptatibus rerum.
				</p>
				<p>
					Fuga cumque veniam, molestiae tenetur veritatis minima
					deleniti corporis dolore vero, impedit voluptatibus
					repellendus esse recusandae facilis numquam sequi laborum
					eveniet, nulla! Corporis dolores ipsa commodi officia nulla
					tenetur doloremque.
				</p>
				<p>
					Ad facilis, error, adipisci optio enim minima id. Pariatur
					et praesentium fugiat esse nisi, modi dolorum odio nobis,
					ducimus vero cum. In ducimus veritatis est distinctio sunt,
					omnis voluptatibus rerum.
				</p>
				<p>
					Fuga cumque veniam, molestiae tenetur veritatis minima
					deleniti corporis dolore vero, impedit voluptatibus
					repellendus esse recusandae facilis numquam sequi laborum
					eveniet, nulla! Corporis dolores ipsa commodi officia nulla
					tenetur doloremque.
				</p>
				<p>
					Ad facilis, error, adipisci optio enim minima id. Pariatur
					et praesentium fugiat esse nisi, modi dolorum odio nobis,
					ducimus vero cum. In ducimus veritatis est distinctio sunt,
					omnis voluptatibus rerum.
				</p>
			</section>
		)
	}
}
