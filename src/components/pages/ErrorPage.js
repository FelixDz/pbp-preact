import { h, Component } from 'preact'
import Helmet from 'preact-helmet'
import { Link } from 'preact-router/match'

export default class ErrorPage extends Component {
	render({ type, url }) {
		let infoMessage = error => {
			switch (error) {
				case '404':
					return "Cette page n'existe pas."
					break
				case '403':
					return "Vous n'avez pas l'autorisation d'accéder à cette page… Essayez la porte d'à côté !"
					break
				default:
					return "Et cette erreur est inconnue… Diantre !"
			}
		}

		return (
			<section class="ErrorPage">
				<Helmet
					title="Erreur"
					meta={[
						{ name: 'robots', content: "noindex,nofollow" }
					]}
					link={[{ rel: 'canonical', href: "https://www.petitebrasseriepicarde.fr/404" }]}
				/>
				<h2>Eh bien ! Un petit souci… (erreur {type})</h2>
				<p>Vous avez tenté d'accéder à l'url suivante : {url}</p>
				<p>{infoMessage(type)} Si cela persiste, <Link href="mailto:contact@petitebrasseriepicarde.fr" title="" alt="Signaler le problème par mail">prévenez-nous</Link> !</p>
			</section>
		)
	}
}
