import { h, Component } from 'preact'
import Helmet from 'preact-helmet'
import { Link } from 'preact-router/match'

import Spacer from '@/global/spacer/Spacer'
import SecondaryLink from '@/global/links/secondary/SecondaryLink'

import News from 'async!@/news/News'
import Reviews from 'async!@/reviews/Reviews'

export default class HomePage extends Component {

	constructor (props) {
		super(props)
		this.metas = {
			url: 'https://www.petitebrasseriepicarde.fr',
			image: 'https://picsum.photos/200',
			description:
				'La Petite Brasserie Picarde est une brasserie artisanale proposant des bières biologiques de caractères. Située à Grandfresnoy (Oise, Picardie), elle propose régulièrement des animations et visites sur place.'
		}
	}

	render() {
		return (
			<section class="HomePage">
				<Helmet
					// title={this.metas.title}
					meta={[
						{ name: 'description', content: this.metas.description },
						// Facebook meta
						// { property: 'og:title', content: this.metas.title },
						{ property: 'og:type', content: 'article' },
						{ property: 'og:description', content: this.metas.description },
						{ property: 'og:url', content: this.metas.url },
						{ property: 'og:site_name', content: 'Petite Brasserie Picarde' },
						{ property: 'og:image', content: this.metas.image },
						{ property: 'og:image:secure_url', content: this.metas.image },
						// Twitter meta
						// { name: 'twitter:title', content: this.metas.title },
						{ name: 'twitter:creator', content: 'Petite Brasserie Picarde' },
						{ name: 'twitter:site', content: 'https://www.petitebrasseriepicarde.fr/' },
						{ name: 'twitter:card', content: 'summary' },
						{ name: 'twitter:image', content: this.metas.image },
						{ name: 'twitter:description', content: this.metas.description }
					]}
					link={[{ rel: 'canonical', href: this.metas.url }]}
				/>

				<h1>Bienvenue !</h1>

				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut
					sed sollicitudin mauris. Aenean vitae congue ligula.
					Maecenas congue dapibus dolor, et porta mi tempus et. Class
					aptent taciti sociosqu ad litora torquent per conubia
					nostra, per inceptos himenaeos. Maecenas sit amet nisl nec
					lorem <Link href="/test">eleifend pulvinar</Link>. Sed ac nibh sed
					mauris auctor eleifend. Nam enim mi, pulvinar in faucibus
					vel, imperdiet non quam. <br />
					<Link href="/photos">Albums photo</Link> &middot; <Link href="/points-de-vente">Points de vente</Link> &middot; <Link href="/produits">Produits</Link>
				</p>

				<Spacer large />

				<News smallTitle latest />

				<Spacer small />

				<SecondaryLink
					href="/nouvelles"
					title="Voir toutes les nouvelles"
					alt="Voir toutes les nouvelles"
					forward
					centered
				>
					Voir toutes les nouvelles
				</SecondaryLink>

				<Spacer huge />

				<h2>Ce qu'on dit de nous…</h2>

				<Reviews category="general" limit="4" />
				
			</section>
		)
	}
}
