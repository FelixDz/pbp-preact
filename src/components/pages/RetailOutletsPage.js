import { h, Component } from 'preact'
import Helmet from 'preact-helmet'

import Spacer from '@/global/spacer/Spacer'

// Map Widget
import MapWidget from '@/widgets/map/MapWidget'

export default class RetailOutletsPage extends Component {
	constructor (props) {
		super(props)
		this.metas = {
			title: 'Points de vente',
			url: 'https://www.petitebrasseriepicarde.fr/points-de-vente',
			image: 'https://picsum.photos/200',
			description: 'Où acheter ou boire les bières de la Petite Brasserie Picarde ? Découvrez nos points de vente (restaurants, caves à bière, dépôts) en Picardie et ailleurs.'
		}
	}

	render() {
		return (
			<section class="RetailOutletsPage">
				<Helmet
					title={this.metas.title}
					meta={[
						{ name: 'description', content: this.metas.description },
						// Facebook meta
						{ property: 'og:title', content: this.metas.title + ' | Petite Brasserie Picarde' },
						{ property: 'og:type', content: 'article' },
						{ property: 'og:description', content: this.metas.description },
						{ property: 'og:url', content: this.metas.url },
						{ property: 'og:site_name', content: 'Petite Brasserie Picarde' },
						{ property: 'og:image', content: this.metas.image },
						{ property: 'og:image:secure_url', content: this.metas.image },
						// Twitter meta
						{ name: 'twitter:title', content: this.metas.title + ' | Petite Brasserie Picarde' },
						{ name: 'twitter:creator', content: 'Petite Brasserie Picarde' },
						{ name: 'twitter:site', content: 'https://www.petitebrasseriepicarde.fr/' },
						{ name: 'twitter:card', content: 'summary' },
						{ name: 'twitter:image', content: this.metas.image },
						{ name: 'twitter:description', content: this.metas.description }
					]}
					link={[{ rel: 'canonical', href: this.metas.url }]}
				/>
				<h1>Trouver nos bi&egrave;res</h1>

				<p>Pour acheter (ou boire !) les bi&egrave;res de la brasserie, consultez la carte suivante qui recense nos points de vente : caves &agrave; bi&egrave;re &amp; vin, restaurants, d&eacute;p&ocirc;ts&hellip; Et bien s&ucirc;r la brasserie elle-m&ecirc;me !</p>

				<MapWidget />

			</section>
		)
	}
}
