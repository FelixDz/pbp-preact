import { h, Component } from 'preact'
import Helmet from 'preact-helmet'
import { Link } from 'preact-router/match'

import Products from 'async!@/products/Products'

export default class ProductsPage extends Component {
	constructor (props) {
		super(props)
		this.metas = {
			title: 'Nos produits',
			url: 'https://www.petitebrasseriepicarde.fr/products',
			image: 'https://picsum.photos/200',
			description: 'Les produits de la Petite Brasserie Picarde.'
		}
	}

	render(props) {
		return (
			<section class="ProductsPage">
				<Helmet
					title={this.metas.title}
					meta={[
						{ name: 'description', content: this.metas.description },
						// Facebook meta
						{ property: 'og:title', content: this.metas.title + ' | Petite Brasserie Picarde' },
						{ property: 'og:type', content: 'article' },
						{ property: 'og:description', content: this.metas.description },
						{ property: 'og:url', content: this.metas.url },
						{ property: 'og:site_name', content: 'Petite Brasserie Picarde' },
						{ property: 'og:image', content: this.metas.image },
						{ property: 'og:image:secure_url', content: this.metas.image },
						// Twitter meta
						{ name: 'twitter:title', content: this.metas.title + ' | Petite Brasserie Picarde' },
						{ name: 'twitter:creator', content: 'Petite Brasserie Picarde' },
						{ name: 'twitter:site', content: 'https://www.petitebrasseriepicarde.fr/' },
						{ name: 'twitter:card', content: 'summary' },
						{ name: 'twitter:image', content: this.metas.image },
						{ name: 'twitter:description', content: this.metas.description }
					]}
					link={[{ rel: 'canonical', href: this.metas.url }]}
				/>
				{(props.productId)
					? <Products productId={props.productId} />
					: <Products all />
				}
			</section>
		)
	}
}
