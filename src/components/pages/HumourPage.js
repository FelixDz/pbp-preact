import { h, Component } from 'preact'
import Helmet from 'preact-helmet'
import { Link } from 'preact-router/match'

import Image from '@/global/images/Image'

export default class HumourPage extends Component {
	constructor (props) {
		super(props)
		this.metas = {
			title: 'Humour et bière',
			url: 'https://www.petitebrasseriepicarde.fr/humour',
			image: 'https://picsum.photos/200',
			description: 'Le meilleur de l\'humour sur la bière ! Citations, blagues, jeux…'
		}
	}

	render() {
		return (
			<section class="HumourPage">
				<Helmet
					title={this.metas.title}
					meta={[
						{ name: 'description', content: this.metas.description },
						// Facebook meta
						{ property: 'og:title', content: this.metas.title + ' | Petite Brasserie Picarde' },
						{ property: 'og:type', content: 'article' },
						{ property: 'og:description', content: this.metas.description },
						{ property: 'og:url', content: this.metas.url },
						{ property: 'og:site_name', content: 'Petite Brasserie Picarde' },
						{ property: 'og:image', content: this.metas.image },
						{ property: 'og:image:secure_url', content: this.metas.image },
						// Twitter meta
						{ name: 'twitter:title', content: this.metas.title + ' | Petite Brasserie Picarde' },
						{ name: 'twitter:creator', content: 'Petite Brasserie Picarde' },
						{ name: 'twitter:site', content: 'https://www.petitebrasseriepicarde.fr/' },
						{ name: 'twitter:card', content: 'summary' },
						{ name: 'twitter:image', content: this.metas.image },
						{ name: 'twitter:description', content: this.metas.description }
					]}
					link={[{ rel: 'canonical', href: this.metas.url }]}
				/>
				<h1>Un peu d'humour (ou d'humeurs) sur la bière</h1>

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit.
					Magnam voluptatem possimus nobis autem aut sit quas dolor
					nostrum. Aliquid laudantium quo explicabo sequi cumque
					incidunt assumenda, fuga eos non inventore. <Link href="/" title="">Back to home</Link>.
				</p>
				
			</section>
		)
	}
}
