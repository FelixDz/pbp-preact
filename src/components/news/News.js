import { h, Component } from 'preact'

import {capitalizeFirstLetter} from '@/utils/Utils'
import {printCatPlural, printCatSingular, printCatAPIPlural} from '@/utils/NewsUtils'
import Fetcher from '@/global/fetcher/Fetcher'
import NewsItem from './NewsItem'

import style from './News.scss';

// TODO : add RSS feed

export default class News extends Component {

	renderNews = (news) => {
		return (
			<div class={style.News}>
				{
					news.map(newsItem => {
						return <NewsItem key={newsItem.id} category={this.props.category} newsItem={newsItem} />
					})
				}
			</div>
		)
	}

	render(props) {

		return (
			<Fetcher
				query={printCatAPIPlural(props.category) + (props.latest ? "/latest" : "")}
				renderData={this.renderNews}
				loadingText={("Chargement des " + printCatPlural(props.category) + " en cours...")} 
				errorText={("Pas de nouvelle, bonne nouvelle ? Soit il n'y a pas de " + printCatPlural(props.category) + " à partager en ce moment, soit notre serveur (web)… est parti au troquet du coin ! Si le problème persiste, n'hésitez pas à nous le faire savoir.")}
				showErrorTitle
			/>
		)
	}
}
