import { h, Component } from 'preact'
import Markup from 'preact-markup'

import {formatDate} from '@/utils/Utils'
import {printCatPlural, printCatSingular, printCatAPISingular, printCatURL} from '@/utils/NewsUtils'

import Fetcher from '@/global/fetcher/Fetcher'
import Image from '@/global/images/Image'
import SecondaryLink from '@/global/links/secondary/SecondaryLink'

import style from './NewsDetails.scss';

export default class NewsDetails extends Component {

	renderNewsInfo = (item) => {

		let itemInfo

		switch (item.category) {

			case "event":
			case "event_home":

				var dates, hours
				if (item.starting_date != item.ending_date) {
					dates = <span class={style.InfoDate}><span class="icon-calendar"></span> Du <span class={style.InfoEmphasis}>{ formatDate(item.starting_date) }</span> au <span class={style.InfoEmphasis}>{ formatDate(item.ending_date) }</span> {item.hours && <span class={style.InfoHours}>&middot; <span class={style.InfoEmphasis}>{item.hours}</span></span>}</span>
				} else {
					dates = <span class={style.InfoDate}><span class="icon-calendar"></span> Le <span class={style.InfoEmphasis}>{ formatDate(item.starting_date) }</span> {item.hours && <span class={style.InfoHours}>&middot; <span class={style.InfoEmphasis}>{item.hours}</span></span>}</span>
				}

				itemInfo = <span class={style.Info}>{dates} <span class={style.Middot}></span> <span class={style.InfoPlace}><span class="icon-map-marker"></span> À <span class={style.InfoEmphasis}>{ item.place }</span></span></span>
				break;

			case "award":
				itemInfo = <span class={style.Info}><span class={style.InfoAward}><span class="icon-star"></span> Prix & récompenses</span></span>
				break;

			case "product":
				itemInfo = <span class={style.Info}><span class={style.InfoProduct}><span class="icon-plus"></span> Une nouvelle création</span></span>
				break;

			case "general":
			default:
				itemInfo = <span class={style.Info}><span class={style.InfoDate}><span class="icon-caret-right"></span> Posté le { formatDate(item.post_date) }</span></span>
		}

		return itemInfo
	}

	renderNewsDetails = (item) => {

		let isAward = (item.category === "award")

		return (
			<div class={style.NewsDetails}>
				<h1 class={style.NewsDetailsTitle}>{item.title}</h1>
				<h4 class={style.NewsDetailsSubTitle}>{this.renderNewsInfo(item)}</h4>
				{ item.image && 	// Image shown if present
					<Image class={[style.NewsDetailsImage, (isAward ? style.NewsDetailsAwardImage : "")].join(" ")} title={item.title} alt={item.title} src={item.image} databaseImage noPlaceholder={isAward} cover={!isAward} />
				}
				<Markup markup={item.desc} type="html" trim={false} />
				<p><SecondaryLink href={"/" + printCatURL(this.props.category)} title={"Retourner à la liste des " + printCatPlural(this.props.category)} backward >Retourner à la liste des {printCatPlural(this.props.category)}</SecondaryLink></p>
			</div>
		)
	}

	render(props) {

		return (
			<Fetcher
				query={(printCatAPISingular(props.category) + (props.newsId ? ("/" + props.newsId) : ""))}
				renderData={this.renderNewsDetails}
				loadingText={("Chargement " + printCatSingular(props.category) + " en cours...")} 
				errorText={("Un petit souci lors du chargement " + printCatSingular(props.category) + " : le serveur doit être en pause-café, ou bien les données " + printCatSingular(props.category) + " que vous cherchez n'existent plus.")}
				showErrorTitle
			/>
		)
	}
}
