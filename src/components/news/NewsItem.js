import { h, Component } from 'preact'
import { Link } from 'preact-router/match'

import {formatDate, stripHtml} from '@/utils/Utils'
import {printCatURL} from '@/utils/NewsUtils'

import Image from '@/global/images/Image'
import style from './NewsItem.scss'

export default class NewsItem extends Component {

	render (props) {

		let item = props.newsItem

		let itemInfo

		switch (item.category) {

			case "event":
			case "event_home":

				var dates, hours
				if (item.starting_date != item.ending_date) {
					// dates = <span class={style.InfoDate}>Du <span class={style.InfoEmphasis}>{ formatDate(item.starting_date) }</span> au <span class={style.InfoEmphasis}>{ formatDate(item.ending_date) }</span> {item.hours && <span class={style.InfoHours}>&middot; <span class={style.InfoEmphasis}>{item.hours}</span></span>}</span>
					dates = <span class={style.InfoDate}><span class="icon-calendar"></span>Du <span class={style.InfoEmphasis}>{ formatDate(item.starting_date) }</span> au <span class={style.InfoEmphasis}>{ formatDate(item.ending_date) }</span></span>
				} else {
					// dates = <span class={style.InfoDate}>Le <span class={style.InfoEmphasis}>{ formatDate(item.starting_date) }</span> {item.hours && <span class={style.InfoHours}>&middot; <span class={style.InfoEmphasis}>{item.hours}</span></span>}</span>
					dates = <span class={style.InfoDate}><span class="icon-calendar"></span> Le <span class={style.InfoEmphasis}>{ formatDate(item.starting_date) }</span></span>
				}

				itemInfo = <span class={style.Info}>{dates} <span class={style.Middot}></span> <span class={style.InfoPlace}><span class="icon-map-marker"></span> À <span class={style.InfoEmphasis}>{ item.place }</span></span></span>
				break;

			case "award":
				itemInfo = <span class={style.Info}><span class={style.InfoAward}><span class="icon-star"></span> Prix & récompenses</span></span>
				break;

			case "product":
				itemInfo = <span class={style.Info}><span class={style.InfoProduct}><span class="icon-plus"></span> Une nouvelle création</span></span>
				break;

			case "general":
			default:
				itemInfo = <span class={style.Info}><span class={style.InfoDate}><span class="icon-caret-right"></span> Posté le { formatDate(item.post_date) }</span></span>
		}

		return (
			<div class={style.NewsItemContainer}>
				<Link href={"/" + printCatURL(props.category) + "/" + item.id} title={"Lire la suite de \"" + item.title + "\""}>
					<div class={`${style.NewsItem} News-${item.category}`}>
						{ item.image && 	// Image shown if present
							<Image class={style.NewsItemImage} title={item.title} alt={item.title} src={item.image} thumb databaseImage rounded cover />
						}
						<div class={style.NewsItemContent}>
							<h4>{item.title}</h4>
							<h6>{itemInfo}</h6>
							<p>
								{stripHtml(item.desc)}
							</p>
						</div>
					</div>
				</Link>
			</div>
		);
	}
}
