import { h, Component } from 'preact'
import style from './Image.scss'

export default class Image extends Component {

	state = {}

	imageHasLoaded() {
		this.setState({ loaded: true })
	}

	render(props) {
		const imageClasses = [
			props.class,
			style.Image,
			props.rounded ? style.Rounded : '',
			props.smallRounded ? style.SmallRounded : '',
			props.circular ? style.Circular : '',
			props.shadow ? style.Shadow : '',
			props.flatShadow ? style.FlatShadow : '',
			props.innerShadow ? style.InnerShadow : '',
			props.noPlaceholder ? style.NoPlaceholder : '',
			props.cover ? style.Cover : '',
			props.fit ? style.Fit : '',
			props.fullWidth ? style.FullWidth : '',
			this.state.loaded ? style.loaded : ''
		].join(' ')

		let imagePath = props.src

		if (props.databaseImage) {

			if (props.thumb) {
				imagePath = imagePath.replace(/(\.[\w\d_-]+)$/i, '-thumb$1')
			} else if (props.small) {
				imagePath = imagePath.replace(/(\.[\w\d_-]+)$/i, '-small$1')
			}

			imagePath = "https://www.petitebrasseriepicarde.fr/storage/" + imagePath
		}

		return (
			<div class={imageClasses}>
				<img src={imagePath} alt={props.alt} title={props.title} onLoad={this.imageHasLoaded.bind(this)} />
			</div>
		)
	}
}
