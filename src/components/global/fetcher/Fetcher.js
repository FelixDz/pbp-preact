import { h, Component } from 'preact'

import Loader from '@/global/loader/Loader'

import style from './Fetcher.scss'

export default class Photos extends Component {

	constructor() {
		super()
		this.state = {
			data: null
		}
	}

	componentDidMount() {
		this.fetchData(this.props.query)
	}

	componentDidUpdate(prevProps) {
		if (this.props.query !== prevProps.query) {
			this.setState({data: null})
			this.fetchData(this.props.query)
		}
	}

	fetchData = (query) => {
		//fetch('https://pbp-api.test/api/' + query)
		//fetch('/api/data/' + query) // See https://github.com/preactjs/preact-cli/wiki/Config-Recipes#setting-proxy-for-dev-server-using-config-directly
		fetch('https://www.petitebrasseriepicarde.fr/api/data/' + query)
			.then(response => {
				return response.json()
			})
			.then(data => {
				this.setState({ data: data })
			})
			.catch(error => {
				this.setState({ data: {} })
				console.log(error)
			})
	}

	checkData = (data) => {
		// Check if data is not null, and not empty (Array or Object)
		return (data) && ((data.constructor === Array && data.length > 0) || (data.constructor === Object && Object.keys(data).length > 0))
	}

	displayData = (data) => {

		let errorText = this.props.errorText ? this.props.errorText : "Aucune donnée n'a été trouvée. C'est peut-être une erreur de chargement… ou bien il n'y a rien à vous partager en ce moment !"

		if (this.checkData(data)) {

				// Data exists, render it with the appropriate passed method
				return this.props.renderData(data)

			} else {

				// The returned data is empty : there was an error during API fecth or no data exists
				return (
					<div class={style.FetchError}>
						{this.props.showErrorTitle ? <h2 class={style.FetchErrorTitle}>Bloup…</h2> : ''}
						<p class={style.FetchErrorSubtitle}>Nous n'avons trouvé aucun résultat.</p>
						<p>{errorText}</p>
					</div>
				)
			}
	}

	render(props, state) {

		let data = state.data
		let loadingText = props.loadingText ? props.loadingText : "Chargement en cours"

		return (
			<div class={[style.Fetcher, data ? style.DataLoaded : ""].join(" ")}>
				<Loader text={loadingText} loaded={data !== null} />
				<div class={style.FetchedData}>
					{this.displayData(data)}
				</div>
			</div>
		)
	}
}
