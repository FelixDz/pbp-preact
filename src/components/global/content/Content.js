import { h, Component } from 'preact'
import style from './Content.scss'

export default class Content extends Component {
	render({ children }) {
		return <section class={style.Content}>{children}</section>
	}
}
