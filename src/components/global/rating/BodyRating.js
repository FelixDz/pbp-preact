import { h, Component } from 'preact'

import Rating from '@/global/rating/Rating'
import style from './BodyRating.scss'

export default class BodyRating extends Component {

	render(props) {

		let numElems = 5
		let fullElems = [
							<span class={[style.BodyRatingElem, style.BodyRatingElem1, "rating-full"].join(" ")}></span>,
							<span class={[style.BodyRatingElem, style.BodyRatingElem2, "rating-full"].join(" ")}></span>,
							<span class={[style.BodyRatingElem, style.BodyRatingElem3, "rating-full"].join(" ")}></span>,
							<span class={[style.BodyRatingElem, style.BodyRatingElem4, "rating-full"].join(" ")}></span>,
							<span class={[style.BodyRatingElem, style.BodyRatingElem5, "rating-full"].join(" ")}></span>
						]
		let emptyElems = [
							<span class={[style.BodyRatingElem, style.BodyRatingElem1, "rating-empty"].join(" ")}></span>,
							<span class={[style.BodyRatingElem, style.BodyRatingElem2, "rating-empty"].join(" ")}></span>,
							<span class={[style.BodyRatingElem, style.BodyRatingElem3, "rating-empty"].join(" ")}></span>,
							<span class={[style.BodyRatingElem, style.BodyRatingElem4, "rating-empty"].join(" ")}></span>,
							<span class={[style.BodyRatingElem, style.BodyRatingElem5, "rating-empty"].join(" ")}></span>
						]

		return (
		    <span class={style.BodyRating}>
		    	<Rating 
		    		rating={props.rating}
		    		numElems={numElems}
		    		fullElems={fullElems}
		    		emptyElems={emptyElems}
		    	/>
		    </span>
		)
	}
}
