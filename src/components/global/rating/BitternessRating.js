import { h, Component } from 'preact'

import Rating from '@/global/rating/Rating'
import style from './BitternessRating.scss'

export default class BitternessRating extends Component {

	render(props) {

		let numElems = 5
		let fullElems = Array(numElems).fill(<span class={[style.BitternessRatingElem, "rating-full"].join(" ")}></span>)
		let emptyElems = Array(numElems).fill(<span class={[style.BitternessRatingElem, "rating-empty"].join(" ")}></span>)

		return (
		    <span class={style.BitternessRating}>
		    	<Rating 
		    		rating={props.rating}
		    		numElems={numElems}
		    		fullElems={fullElems}
		    		emptyElems={emptyElems}
		    	/>
		    </span>
		)
	}
}
