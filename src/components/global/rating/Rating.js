import { h, Component } from 'preact'

import style from './Rating.scss'

export default class Rating extends Component {

	renderRating = (rating,
	                numElems = 5,
	                fullElems = Array(numElems).fill(<span class="icon-star"></span>),
	                emptyElems = Array(numElems).fill(<span class="icon-star-o"></span>),
	                renderHalfElems = false,
	                halfElem = <span class="icon-star-half-o"></span>
	                ) => {

		if (rating) {

			let elems = new Array();

			// Full elems
			for (var i = 0 ; i < Math.floor(rating) ; i++ ) {
				elems.push(fullElems[i])
			}

			// Half elems
			if ((rating % 1) !== 0) {
				if (renderHalfElems) {
					elems.push(halfElem)
				} else {
					elems.push(fullElems[Math.floor(rating)])
				}
			}

			// Empty elems
			for (var i = 0 ; i < Math.floor(numElems - rating) ; i++ ) {
				elems.push(emptyElems[Math.floor(rating) + Math.ceil(rating % 1) + i])
			}

			return (
				<span class={style.Rating}>
					{ elems }
				</span>
			)
		} else {
			return null
		}
	}

	render(props) {

		return (
			this.renderRating(props.rating, props.numElems, props.fullElems, props.emptyElems, props.renderHalfElems, props.halfElems)
		)
	}
}
