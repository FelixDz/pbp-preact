import { h, Component } from 'preact'
import { Link } from 'preact-router/match'
import style from './ScrollUp.scss'

import {scrollTo} from '@/utils/Utils'

export default class ScrollUp extends Component {

	handleClick (event) {
		event.preventDefault()
		event.stopPropagation()
		scrollTo(0, 300, 'easeOutQuad')
	}

	render() {
		return (
			<Link class={style.ScrollUp} href="" alt="Haut de page" title="Haut de page" onClick={this.handleClick.bind(this)}>
				<svg xmlns="http://www.w3.org/2000/svg" version="1" viewBox="0 0 580 347" class={style.ScrollUpArrow}>
					<path class={style.ScrollUpArrowPath} d="M564 240c11 11 16 24 16 41s-5 30-16 41c-25 25-51 25-78 0L290 134 94 322c-27 25-53 25-78 0-11-11-16-24-16-41s5-30 16-41L250 16c11-11 24-16 40-16s29 5 40 16l234 224"/>
				</svg>
			</Link>
		)
	}
}
