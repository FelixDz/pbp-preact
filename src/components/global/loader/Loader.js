import { h, Component } from 'preact'

import style from './Loader.scss'

export default class Loader extends Component {

	render(props) {
		return (
			<div class={[style.Loader, props.loaded ? style.Loaded : ""].join(" ")}>
				<div class={style.LoaderImage}></div>
				{ props.text &&
					<p class={style.LoaderText}>{props.text}</p>
				}
			</div>
		)
	}
}
