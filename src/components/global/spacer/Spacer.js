import { h, Component } from 'preact'
import style from './Spacer.scss'

export default class Spacer extends Component {
	render(props) {

		let classes = [style.Spacer, 'Spacer', props.large ? style.large : '', props.huge ? style.huge : '', props.small ? style.small : ''].join(' ')

		return (
			<section class={classes}>
			</section>
		)
	}
}
