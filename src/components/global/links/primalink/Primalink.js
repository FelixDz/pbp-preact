import { h, Component } from 'preact'
import { Link } from 'preact-router/match'
import style from './Primalink.scss'

// Use example :
// import Primalink from './../primalink/Primalink';
// <Primalink path="/" alt="" title="" caption="Some caption" subcaption={["Subcaption please ?"]} isPrimary />

export default class Primalink extends Component {
	render(props) {
		let classes = [style.Primalink, 'Primalink', props.isPrimary ? style.primary : ''].join(' ')

		return (
			<Link href={props.path}
				alt={props.alt ? props.alt : props.caption}
				title={props.title ? props.title : props.caption}
				class={classes}
			>
				<div onClick={(e) => (typeof props.onClick !== 'undefined') ? props.onClick(e) : null} class={style.PrimalinkContent}>
					<span class={style.PrimalinkCaption}>{props.caption}</span>
					{props.subcaption &&
						props.subcaption.length !== 0 && (
							<span class={style.PrimalinkSubcaption}>
								{props.subcaption.map(subcap => {
									return <span key={subcap}>{subcap}</span>
								})}
							</span>
						)}
				</div>
			</Link>
		)
	}
}
