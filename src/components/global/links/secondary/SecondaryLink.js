import { h, Component } from 'preact'
import { Link } from 'preact-router/match'
import style from './SecondaryLink.scss'

export default class SecondaryLink extends Component {

	render(props) {

		let classes = [style.SecondaryLink, 'SecondaryLink', props.backward ? style.backward : '', props.forward ? style.forward : '', props.centered ? style.centered : ''].join(' ')

		return (
			<Link href={props.href}
				alt={props.alt ? props.alt : props.href}
				title={props.title ? props.title : props.href}
				class={classes}
			>
				<div onClick={(e) => (typeof props.onClick !== 'undefined') ? props.onClick(e) : null} class={style.SecondaryLinkContent}>
					{props.children}
				</div>
			</Link>
		)
	}
}
