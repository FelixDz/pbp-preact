import { h, Component } from 'preact'
import { Link } from 'preact-router/match'
import style from './Nav.scss'

import NavSections from './NavSections'
import NavDetails from './NavDetails'
import NavMobileBurger from './NavMobileBurger'

class Nav extends Component {

	state = {
		isMenuOpen: false,
		activeSection: 1
	}

	handleMenuOpen () {
		this.setState({ isMenuOpen: true })
	}

	handleMenuClose () {
		this.setState({ isMenuOpen: false })
	}

	handleMenuToggle () {
		if (this.state.isMenuOpen) {
			this.handleMenuClose()
			const self = this
			// Reset the active section to default for mobile
			setTimeout(() => self.setState({activeSection: 1}), 500)
		} else {
			this.handleMenuOpen()
		}
	}

	handleActiveSection (id) {
		this.setState({ isMenuOpen: true, activeSection: id })
	}

	render(props, state) {

		let navClasses = [style.Nav, state.isMenuOpen ? 'menuOpen' : ''].join(' ')

		let activeSectionData = props.menu.sections.concat(props.menu.home).find(sec => sec.id === state.activeSection)

		return (
			<nav class={navClasses}>
				<div class={style.NavMain}>

					<Link class={ [style.HomeLink, style.MobileNavButton].join(' ') } href={props.menu.home.path} title={props.menu.home.name} onMouseEnter={() => this.handleActiveSection(1)} onClick={this.handleMenuToggle.bind(this)}>
						<svg xmlns="http://www.w3.org/2000/svg" width="1971.01" height="2162.96" viewBox="0 0 1847.83 2027.78" class={style.HomeLogo}>
						<g>
							<g class={style.LogoPBPbat} fill="#ffffff">
								<path d="M690.6 35.43l28.1 193.98-48.2 64.8 12.14 5.9L923.9 416.8l253.44-122.56-48.22-64.76 28.1-193.92L923.92 187zM611.23 365.9C529.7 654.04 430.8 756.3 404.33 778.76l-4.25 3.6-.15 5.6c-1.45 52.87 9.22 128.08 44.77 182.04l4.82 7.33 8.53-2.05c83.53-20.06 130.95-49.45 178.8-93.96l5.62-5.23-2.14-7.4c-7.12-24.6-21.45-63.9-27.74-140.3-6.3-76.4-4.3-189.3 23-357.2zM477.8 39.3l-40.46 17.26C181.1 165.96 63.14 394.52 41.38 562.86 19.8 729.73 61.95 850.9 87.58 898.46l13.47 25 9.34-26.8c10.5-30.2 33.9-59.84 72.8-80.03 19.2-9.97 54.3-22.8 92.8-40.52 38.4-17.7 80.5-40.5 115.3-72.3 70.5-64.5 140.3-210.7 173.4-344.2l3.7-15.1-15.6-.4c-54.7-1.3-102.2-36-125-86.7-22.9-50.7-21.3-116.9 24.6-182z" overflow="visible" />
								<path d="M684.57 360.64l-3.74 14.08S646.77 502.6 659.07 660.4s71.56 347.44 258.06 468.03l6.78 4.38 6.8-4.3c186.5-120.6 245.8-310.2 258.1-468 12.3-157.8-21.7-285.6-21.7-285.6l-3.7-14.1-13.3 5.9-226 98.8zm552.03 5.26l-24.37 5.4c27.24 167.87 29.3 280.72 23 357.14-6.3 76.4-20.62 115.66-27.74 140.26l-2.2 7.4 5.6 5.22c47.8 44.5 95.2 73.9 178.8 93.96l8.5 2.05 4.8-7.33c35.5-53.96 46.2-129.17 44.7-182.05l-.2-5.58-4.3-3.6c-26.4-22.47-125.3-124.73-206.88-412.87zm133.42-326.6l25.34 35.96c45.88 65.13 47.47 131.37 24.6 182.06-22.86 50.7-70.32 85.4-125 86.74l-15.56.38 3.74 15.12c33.02 133.54 102.88 279.7 173.4 344.22 34.75 31.8 76.84 54.62 115.27 72.34 38.5 17.72 73.7 30.55 92.9 40.52 38.9 20.2 62.3 49.82 72.8 80.02l9.4 26.8 13.5-24.98c25.7-47.58 67.8-168.74 46.2-335.63-21.7-168.33-139.7-396.88-395.9-506.3z" overflow="visible" />
							</g>
							<path class={style.LogoPBPhop} fill="#87ae3c" d="M1508.92 806.1l14.1 5.65s74.93 30.05 146.26 65.72l6.1 3.05.74 6.78c11.42 105.64 11.7 227.57-29.6 402.78l-2.88 12.13-12.14-2.8c-210.87-49.1-418.6-216.2-492.17-286.9l-8.05-7.7 6.78-8.9c12.95-16.9 25.73-34 34.52-51.9l6.04-12.2 11.68 7.1c71.12 43.4 157.43 84.9 252.5 109 30.6-58 48.46-148.5 63.3-226.7zm-402.7 234.04l9.9 11.03c22.66 25.23 86.27 75.44 157.2 125.6 70.95 50.17 150 101.32 205.8 132.02l5.3 2.8 1.03 5.9c19.1 110.1-16.32 230.7-99.43 358.2l-5.54 8.5-9.48-3.7c-84.64-33.2-176.87-110.3-254.5-199.7-77.6-89.4-140.35-190.9-163.13-275.4l-2.5-9.3 8.36-4.8c3-1.7 11.48-8.2 21.65-17.1 10.17-9 22.57-20.6 35.86-33.8 26.58-26.4 56.75-59.1 80.33-88.9zM338.9 806.1l-14.1 5.65s-74.93 30.05-146.26 65.72l-6.1 3.05-.74 6.78c-11.42 105.64-11.7 227.57 29.62 402.78l2.86 12.13 12.14-2.8c210.88-49.1 418.6-216.2 492.17-286.9l8-7.7-6.8-8.9c-13-16.9-25.7-34-34.5-51.9l-6.1-12.3-11.7 7.1c-71.2 43.4-157.5 84.9-252.5 109-30.6-58.1-48.5-148.5-63.3-226.7zm402.7 234.04l-9.92 11.03c-22.64 25.23-86.25 75.44-157.2 125.6-70.93 50.17-150 101.32-205.8 132.02l-5.26 2.8-1.03 5.9c-19.1 110.1 16.3 230.7 99.4 358.2l5.5 8.5 9.4-3.7c84.6-33.2 176.8-110.3 254.5-199.7 77.6-89.5 140.3-190.9 163.1-275.4l2.5-9.3-8.4-4.8c-3-1.7-11.5-8.2-21.7-17.1-10.17-9-22.57-20.6-35.86-33.8-26.53-26.4-56.7-59.1-80.3-88.9zM923.9 1265l-11.83 35.12c-16.74 49.63-67.63 124.1-118.96 189.05-51.3 64.95-103.7 121.84-121.4 138.18l-5.6 5.2 2.1 7.34c40.5 144.6 139.3 275 248.9 347.8l6.9 4.6 7-4.6c109.6-72.8 208.4-203.2 248.9-347.9l2.1-7.4-5.6-5.2c-17.65-16.4-70.1-73.2-121.44-138.2-51.3-65-102.2-139.4-118.93-189.1z" overflow="visible" />
						</g>
						</svg>
					</Link>

					<NavMobileBurger isMenuOpen={state.isMenuOpen} toggleMenu={this.handleMenuToggle.bind(this)} />

					<NavSections
						setActiveSection={sectionID => this.handleActiveSection(sectionID)}
						sections={props.menu.sections}
						isMenuOpen={state.isMenuOpen}
						activeSection={state.activeSection}
						closeMenu={this.handleMenuClose.bind(this)}
					/>

					<a href="https://www.facebook.com/petitebrasseriepicarde/" target="_blank" rel="noreferrer" title="Retrouvez la brasserie sur Facebook" class={ [style.FacebookLink, style.MobileNavButton].join(' ') }>
						<img src={require('@assets/nav/icon_facebook_hover.png').default} title="Retrouvez la brasserie sur Facebook" alt="Logo Facebook" class={style.FacebookLogo} />
					</a>

				</div>

				<div class={style.NavBackground}></div>
				<div class={style.NavContentOverlay} onMouseEnter={this.handleMenuClose.bind(this)}></div>
			</nav>
		);
	}
}

Nav.defaultProps = {
	menu: {
		home: {
			id: 0,
			name: 'Accueil',
			path: '/'
		},
		sections: [
			{
				id: 1,
				name: 'Nos bières',
				description: 'Elles sont belles, artisanales, bio et surtout brassées avec amour, nous vous présentons...',
				logo: {
					name: 'Nos bières',
					url: 'icon_beer_hover.png'
				},
				images: [
					{
						name: 'Dummy image',
						url: 'Bieres_01.png'
					},
					{
						name: 'Dummy image',
						url: 'Bieres_02.png'
					},
					{
						name: 'Dummy image',
						url: 'Bieres_03.png'
					},
					{
						name: 'Dummy image',
						url: 'Bieres_04.png'
					},
					{
						name: 'Dummy image',
						url: 'Bieres_05.png'
					}
				],
				subsections: [
					{
						id: 11,
						name: 'Découvrir notre gamme',
						path: '/produits',
					},
					{
						id: 12,
						name: 'Notre éthique',
						path: '/ethique',
					},
					{
						id: 13,
						name: 'Nos produits récompensés',
						path: '/recompenses',
					},
					{
						id: 14,
						name: 'Trouver nos bières',
						path: '/points-de-vente',
					}
				]
			},
			{
				id: 2,
				name: 'La brasserie',
				description: 'Nos bières y sont confectionnées, et vous pouvez bien sûr faire une visite ou bien déguster un verre autour d\'une assiette de charcuterie ! Découvrez...',
				logo: {
					name: 'Brasserie',
					url: 'icon_brewery_hover.png'
				},
				images: [
					{
						name: 'Dummy image',
						url: 'Bieres_03.png'
					},
					{
						name: 'Dummy image',
						url: 'Bieres_04.png'
					},
					{
						name: 'Dummy image',
						url: 'Bieres_05.png'
					}
				],
				subsections: [
					{
						id: 21,
						name: 'Notre histoire',
						path: '/histoire',
					},
					{
						id: 22,
						name: 'Boire (et manger) sur place',
						path: '/biergarten',
					},
					{
						id: 23,
						name: 'Venir visiter',
						path: '/visites',
					}
				]
			},
			{
				id: 3,
				name: 'Évènements',
				description: 'À domicile ou en déplacement, nous aimons partager de bons moments avec vous ! Retrouvez-nous sur nos...',
				logo: {
					name: 'Évènements',
					url: 'icon_animations_hover.png'
				},
				images: [
					{
						name: 'Dummy image',
						url: 'Bieres_01.png'
					},
					{
						name: 'Dummy image',
						url: 'Bieres_02.png'
					},
					{
						name: 'Dummy image',
						url: 'Bieres_03.png'
					},

					{
						name: 'Dummy image',
						url: 'Bieres_05.png'
					}
				],
				subsections: [
					{
						id: 31,
						name: 'À la brasserie',
						path: '/evenements-brasserie',
					},
					{
						id: 32,
						name: 'Sur les marchés, fêtes…',
						path: '/evenements-ailleurs',
					},
					{
						id: 33,
						name: 'Tous les évènements',
						path: '/evenements',
					}
				]
			},
			{
				id: 4,
				name: 'Galerie',
				description: 'Pour nous découvrir un peu mieux, avoir un aperçu du lieu avant d\'y passer ou encore retrouver les souvenirs d\'un instant partagé avec nous, retrouvez la brasserie dans la...',
				logo: {
					name: 'Galerie',
					url: 'icon_tavern_hover.png'
				},
				images: [
					{
						name: 'Dummy image',
						url: 'Bieres_01.png'
					},
					{
						name: 'Dummy image',
						url: 'Bieres_04.png'
					},
					{
						name: 'Dummy image',
						url: 'Bieres_05.png'
					}
				],
				subsections: [
					{
						id: 41,
						name: 'Albums photos',
						path: '/photos',
					},
					{
						id: 42,
						name: 'Vidéos',
						path: '/videos',
					},
					{
						id: 43,
						name: 'Humour brassicole',
						path: '/humour',
					},
					{
						id: 44,
						name: 'Il·elles ont laissé leur avis',
						path: '/avis',
					}
				]
			},
			/*
			{
				id: 5,
				name: 'Repas',
				path: '/',
				description: 'Nos bières y sont confectionnées, mais vous pouvez aussi venir faire une visite ou bien déguster un verre autour d\'une assiette de charcuterie. Découvrez la...',
				logo: {
					name: 'Repas',
					url: 'icon_eat_hover.png'
				},
				images: [
					{
						name: 'Dummy image',
						url: 'Bieres_01.png'
					}
				],
				subsections: [
					{
						id: 51,
						name: 'Toujours pluuus',
						path: '/',
					}
				]
			},
			*/
			{
				id: 5,
				name: 'Contact',
				description: 'Vous avez besoin de renseignements, une envie d\'en savoir plus, ou de travailler avec nous ? Ou tout simplement de nous faire parvenir quelques mots ? Prenez...',
				logo: {
					name: 'Contact',
					url: 'icon_contact_hover.png'
				},
				images: [
					{
						name: 'Dummy image',
						url: 'Bieres_01.png'
					},
					{
						name: 'Dummy image',
						url: 'Bieres_02.png'
					},
					{
						name: 'Dummy image',
						url: 'Bieres_03.png'
					},
					{
						name: 'Dummy image',
						url: 'Bieres_04.png'
					},
					{
						name: 'Dummy image',
						url: 'Bieres_05.png'
					}
				],
				subsections: [
					{
						id: 51,
						name: 'Nous contacter',
						path: '/contact',
					},
					{
						id: 52,
						name: 'Devenir partenaires professionnels',
						path: '/pros',
					}
				]
			}
		]
	}
};

export default Nav;
