import { h, Component } from 'preact'
import style from './NavMobileBurger.scss'

export default class NavMobileBurger extends Component {
	render(props) {
		let navBurgerClasses = [
			style.NavMobileBurger,
			style.MobileNavButton,
			props.isMenuOpen ? style.navBurgerCollapse : ''
		].join(' ')

		return (
			<div class={navBurgerClasses} onClick={props.toggleMenu}>
				<div class={style.NavMobileBurgerLogo}>
					<div class={style.NavMobileBurgerX} />
					<div class={style.NavMobileBurgerY} />
					<div class={style.NavMobileBurgerZ} />
				</div>
			</div>
		)
	}
}
