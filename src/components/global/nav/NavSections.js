import { h, Component } from 'preact'
import style from './NavSections.scss'

import NavDetails from './NavDetails'

export default class NavSections extends Component {

	render(props) {

		let navSections = props.sections.map(navSection => {
			let sectionIsActive =
				navSection.id === props.activeSection &&
				props.isMenuOpen
					? style.sectionIsActive
					: ''

			return (
				<li class={style.NavSection} key={navSection.id}>
					<div class={sectionIsActive} onMouseEnter={ () => props.setActiveSection(navSection.id) }>
						<img
							class={style.NavSectionLogo}
							title={navSection.logo.name}
							alt={navSection.logo.name}
							src={require('@assets/nav/' + navSection.logo.url).default}
						/>
						<span class={style.NavSectionTitle}>
							{navSection.name}
						</span>
						<NavDetails className={style.NavSectionDetails}
							sectionData={navSection}
							closeMenu={props.closeMenu}
						/>
					</div>
				</li>
			)
		})

		return <ul class={style.NavSections}>{navSections}</ul>
	}
}
