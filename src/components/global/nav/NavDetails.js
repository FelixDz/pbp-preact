import { h, Component } from 'preact'
import { Link } from 'preact-router/match'

import Image from '@/global/images/Image'

import style from './NavDetails.scss'

export default class NavDetails extends Component {

	renderSectionImages = (images) => {

		return (
			<div class={style.NavDetailsImages}>
				{
					images.slice(0, 6).map(image => {
						return (
							<Image
								class={style.NavDetailsImage} innerShadow circular
								title={image.name}
								alt={image.name}
								src={require('@assets/nav/' + image.url).default}
							/>
						)
					})
				}
			</div>
		)
	}

	render(props) {

		return (
			<div class={[style.NavDetails, props.className].join(" ")} key={props.sectionData.id}>
				<div class={style.NavDetailsDesc}>
					<p>{props.sectionData.description}</p>
				</div>
				<div class={style.NavDetailsContent}>
					<div class={style.NavDetailsLinks}>
						<h2 class={style.NavDetailsTitle}>
							{props.sectionData.name}
						</h2>
						<ul class={style.NavDetailsSubsections}>
							{props.sectionData.subsections &&
								props.sectionData.subsections.map( subsection => {
									return (
										<li class={style.NavDetailsSubsection} key={subsection.id} >
											<Link href={subsection.path} title={subsection.name} onClick={props.closeMenu}>
												{subsection.name}
											</Link>
										</li>
									)
								})}
						</ul>
					</div>

					{ props.sectionData.images &&
						this.renderSectionImages(props.sectionData.images)
					}
				</div>
			</div>
		)
	}
}
