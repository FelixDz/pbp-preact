import { h, Component } from 'preact'
import style from './SearchBar.scss'

import TextInput from '@/global/searchbar/SearchTextInput'
import Button from '@/global/searchbar/SearchButton'

export default class SearchBar extends Component {

	static defaultProps = {
		onCloseClick: () => {},
		onSearch: () => {},
		inputClass: '',
		searchPlaceHolder: 'Rechercher...',
		value: '',
		dynamicSearchStartsFrom: 1,
		allowEmptySearch: false,
	}

	constructor(props) {
		super(props)
		this.state = this.getState()
	}

	componentWillReceiveProps = (nextProps) => {
		if (nextProps.value !== this.props.value) {
			this.setState(this.getState(nextProps))
		}
	}

	onSearch = () => {
		this.props.onSearch(this.state.value)
	}

	onDynamicSearch = (e) => {

		const value = (e.target.value || '')

		this.setState(this.getState(this.props, value))

		if (this.props.dynamicSearchStartsFrom <= value.length || !value) {
			this.props.onSearch(value)
		}
	}

	onCloseClick = () => {
		const value = ''
		this.setState(this.getState(this.props, value))
		this.props.onSearch('')
		this.props.onCloseClick()
	}

	onChange = (e) => {
		const value = (e.target.value || '')
		this.setState(this.getState(this.props, value))
	}

	onKeyDown = (e) => {
		if (e.keyCode && e.keyCode === 13) {
			this.onSearch()
		}
	}

	getState = (props = this.props, value = props.value) => {
		const onChange = props.dynamicSearchStartsFrom ? this.onDynamicSearch : this.onChange
		const onClick = (value && props.dynamicSearchStartsFrom) ? this.onCloseClick : this.onSearch
		const onKeyDown = !props.dynamicSearchStartsFrom ? this.onKeyDown : () => {}
		const disabled = !value
		if(document.querySelector("."+this.props.inputClass)) {
			document.querySelector("."+this.props.inputClass).setAttribute("value", value)
		}
		return {
			onChange,
			onKeyDown,
			onClick,
			value,
			disabled,
		}
	}

	getIcon = () => (
		this.state.value && this.props.dynamicSearchStartsFrom ? <div class={style.ButtonClose}><span class="icon-times"></span></div> : <div class={style.ButtonSearch}><span class="icon-search"></span></div>
	)

	render(props, state) {

		return (
			<form class={[style.SearchBar, props.className].join(" ")}>
				<TextInput className={this.props.inputClass} onInput={e => this.state.onChange(e)} onChange={e => this.state.onChange(e)} onKeyDown={this.state.onKeyDown} value={this.state.value} placeholder={this.props.searchPlaceHolder} transparent />
				<Button className={style.SearchBarButton} onClick={e => this.state.onClick(e)} disabled={!this.props.allowEmptySearch && this.state.disabled} transparent >
					{this.getIcon()}
				</Button>
			</form>
		)
	}
}
