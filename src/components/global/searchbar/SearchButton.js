import { h, Component } from 'preact'
import style from './SearchButton.scss'

export default class SearchButton extends Component {

	render({ className = "", transparent = false, ...props }) {

		let classes = [style.SearchButton, className, transparent ? style.transparent : ''].join(' ')

		return (
			<button type="button" {...props} class={classes}>
				{props.children}
			</button>
		)
	}
}
