import { h, Component } from 'preact'
import style from './SearchTextInput.scss'

export default class SearchTextInput extends Component {

	render({ className = "", transparent = false, ...props }) {

		let classes = [style.SearchTextInput, className, transparent ? style.transparent : '', "browser-default"].join(' ')

		return (
			<input type="text" {...props} class={classes} />
		)
	}
}
