import { h, Component } from 'preact'
import style from './Sidebar.scss'

// Using preact-router Match to hide sidebar on mobile if not on Homepage
import Match from 'preact-router/match'
import Router from 'preact-router'

// Widgets
import OpeningHoursWidget from '@/widgets/opening-hours/OpeningHoursWidget'
import AwardsWidget from '@/widgets/awards/AwardsWidget'
import NewsletterWidget from '@/widgets/newsletter/NewsletterWidget'

export default class Sidebar extends Component {
	render () {
		return (
			<Match path="/">
				{ ({ matches, path, url }) => (
					<section class={[style.Sidebar, (matches ? style.IsRoot : "")].join(" ")}>
						<OpeningHoursWidget />
						<AwardsWidget />
						<NewsletterWidget />
					</section>
				) }
			</Match>
		)
	}
}
