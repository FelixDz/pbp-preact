import { h, Component } from 'preact'
import { Link } from 'preact-router/match'

import style from './Footer.scss'

export default class Footer extends Component {
	render() {
		return (
			<section class={style.Footer}>
				<p>Concocté avec <span class={["icon-heart", style.FooterHeart].join(" ")}></span> à la Bronciade par <Link href="mailto:flisk@zaclys.net" title="" alt="">Flisk</Link></p>
			</section>
		)
	}
}
