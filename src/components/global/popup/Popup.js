import { h, Component } from 'preact'
import { Link } from 'preact-router/match'
import { createPortal } from 'preact/compat'

import style from './Popup.scss'

export default class Popup extends Component {

	handleClick (event) {
		event.preventDefault()
		event.stopPropagation()
		this.props.onDismiss()
	}

	render(props) {
		if (typeof window !== "undefined") {
			return createPortal(
				<div class={[style.Popup, props.isOpen ? 'isOpen' : '', props.primary ? 'primary' : ''].join(" ")}>
					<div class={style.PopupBackdrop} onClick={props.onDismiss} />
					<div class={style.PopupPanel}>
						{props.title &&
							<div class={style.PopupTitle}>{props.title}</div>
						}
						<div class={style.PopupContent}>
							{props.children}
						</div>
						<Link class={style.PopupCloseButton} href="" alt="Fermer" title="Fermer" onClick={this.handleClick.bind(this)}>
							<span class="icon-close"></span>
						</Link>
					</div>
				</div>,
				document.body
			);
		} else {
			return null
		}
	}
}
