import { h, Component } from 'preact'
import style from './Header.scss'

export default class Header extends Component {
	render() {
		return (
			<section class={style.Header} id="Header">
				<div class={style.HeaderText}>
					<h1 class={style.HeaderTitle}>Petite Brasserie Picarde</h1>
					<h3 class={style.HeaderSubtitle}>Bières biologiques de caractère</h3>
				</div>
			</section>
		)
	}
}
