export function debounce (func, wait = 250, immediate = false) {

	var timeout

	return function () {
		var context = this, args = arguments
		var later = function() {
			timeout = null
			if (!immediate) func.apply(context, args)
		};
		var callNow = immediate && !timeout
		clearTimeout(timeout)
		timeout = setTimeout(later, wait)
		if (callNow) func.apply(context, args)
	}
}

export function formatDate (date) {
	let formattedDate = new Date(date)
	formattedDate = formattedDate.toLocaleDateString("fr-FR", {day: 'numeric', month: 'long', year: 'numeric'})
	return formattedDate
}

export function capitalizeFirstLetter (str) {
	return str.charAt(0).toUpperCase() + str.slice(1)
}

export function slugify (str, toLowercase = true) {

	str = str.replace(/^\s+|\s+$/g, '')

    // Make the string lowercase
    toLowercase && (str = str.toLowerCase())

    // Remove accents, swap ñ for n, etc
    let from = "ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆÍÌÎÏŇÑÓÖÒÔÕØŘŔŠŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/_,:;"
    let to   = "AAAAAACCCDEEEEEEEEIIIINNOOOOOORRSTUUUUUYYZaaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa------"
    for (var i = 0, l = from.length ; i < l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i))
    }

    // Remove invalid chars
    str = str
    	.replace(toLowercase ? /[^a-z0-9 -]/g : /[^A-Za-z0-9 -]/g, '') 
	    // Collapse whitespace and replace by -
	    .replace(/\s+/g, '-') 
	    // Collapse dashes
	    .replace(/-+/g, '-')

    return str
}

export function objectContainsValue (obj, value) {

	return Object.keys(obj).some(key => {
		if (obj[key] === null) {
			return false
		} else if (typeof obj[key] === 'object') {
			return objectContainsValue(obj[key], value)
		} else {
			return obj[key].toString().toLowerCase().search(value.toLowerCase()) !== -1
		}
	})
}

export function stripHtml (html){
    // Create a new div element
    let temporalDivElement = document.createElement("div")
    // Set the HTML content with the providen
    temporalDivElement.innerHTML = html
    // Retrieve the text property of the element (cross-browser support)
    return temporalDivElement.textContent || temporalDivElement.innerText || ""
}

export function scrollTo (destination, duration = 200, easing = 'easeOutQuad', callback) {

	// Added wrapper for SSR
	if (typeof window !== "undefined") {

		const easings = {
			linear(t) {
				return t
			},
			easeInQuad(t) {
				return t * t
			},
			easeOutQuad(t) {
				return t * (2 - t)
			},
			easeInOutQuad(t) {
				return t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t
			},
			easeInCubic(t) {
				return t * t * t
			},
			easeOutCubic(t) {
				return --t * t * t + 1
			},
			easeInOutCubic(t) {
				return t < 0.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1
			},
			easeInQuart(t) {
				return t * t * t * t
			},
			easeOutQuart(t) {
				return 1 - --t * t * t * t
			},
			easeInOutQuart(t) {
				return t < 0.5 ? 8 * t * t * t * t : 1 - 8 * --t * t * t * t
			},
			easeInQuint(t) {
				return t * t * t * t * t
			},
			easeOutQuint(t) {
				return 1 + --t * t * t * t * t
			},
			easeInOutQuint(t) {
				return t < 0.5 ? 16 * t * t * t * t * t : 1 + 16 * --t * t * t * t * t
			}
		}

		function getTop () {
			return window.pageYOffset || document.documentElement.scrollTop
		}

		const start = getTop()
		const startTime = 'now' in window.performance ? performance.now() : new Date().getTime()

		const documentHeight = Math.max(
			document.body.scrollHeight,
			document.body.offsetHeight,
			document.documentElement.clientHeight,
			document.documentElement.scrollHeight,
			document.documentElement.offsetHeight
		)
		const windowHeight = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight
		const destinationOffset = typeof destination === 'number' ? destination : destination.offsetTop
		const destinationOffsetToScroll = Math.round(
			documentHeight - destinationOffset < windowHeight ? documentHeight - windowHeight : destinationOffset
		)

		if ('requestAnimationFrame' in window === false) {
			window.scroll(0, destinationOffsetToScroll)
			if (callback) {
				callback()
			}
			return
		}

		function scroll() {
			const now = 'now' in window.performance ? performance.now() : new Date().getTime()
			const time = Math.min(1, (now - startTime) / duration)
			const timeFunction = easings[easing](time)
			window.scroll(0, Math.ceil(timeFunction * (destinationOffsetToScroll - start) + start))

			if (getTop() === destinationOffsetToScroll) {
				if (callback) {
					callback()
				}
				return
			}

			requestAnimationFrame(scroll)
		}

		scroll()
	}
}
