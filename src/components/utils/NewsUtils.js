export function printCatPlural (cat) {
	switch (cat) {
		case "award":
			return "prix & récompenses"
			break
		case "event":
		case "event_elsewhere":
			return "évènements"
			break
		case "event_home":
			return "animations"
			break
		case "product":
			return "derniers produits"
			break
		case "general":
		default:
			return "dernières nouvelles"
	}
}

export function printCatSingular (cat) {
	switch (cat) {
		case "award":
			return "de la récompense"
			break
		case "event":
		case "event_elsewhere":
			return "de l'évènement"
			break
		case "event_home":
			return "de l'animation"
			break
		case "product":
			return "de l'annonce de produit"
			break
		case "general":
		default:
			return "de la nouvelle"
	}
}

export function printCatAPIPlural (cat) {
	switch (cat) {
		case "award":
			return "awards"
			break
		case "event":
			return "events"
			break
		case "event_elsewhere":
			return "events-elsewhere"
			break
		case "event_home":
			return "events-home"
			break
		case "product":
			return "last-products"
			break
		case "general":
		default:
			return "news"
	}
}

export function printCatAPISingular (cat) {
	switch (cat) {
		case "award":
			return "award"
			break
		case "event":
			return "event"
			break
		case "event_elsewhere":
			return "event-elsewhere"
			break
		case "event_home":
			return "event-home"
			break
		case "product":
			return "last-product"
			break
		case "general":
		default:
			return "news"
	}
}

export function printCatURL (cat) {
	switch (cat) {
		case "award":
			return "recompenses"
			break
		case "event":
			return "evenements"
			break
		case "event_elsewhere":
			return "evenements-ailleurs"
			break
		case "event_home":
			return "evenements-brasserie"
			break
		case "product":
			return "derniers-produits"
			break
		case "general":
		default:
			return "nouvelles"
	}
}
