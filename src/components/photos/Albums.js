import { h, Component } from 'preact'

import Fetcher from '@/global/fetcher/Fetcher'
import Album from '@/photos/Album'

import style from './Albums.scss'

export default class Albums extends Component {
	
	renderAlbums = (albums) => {

		return (
			<div class={style.AlbumsList}>
				{
					albums.map(album => {
						return <Album key={album.slug} albumData={album} />
					})
				}
			</div>
		)
	}

	render(props) {

		return (
			<div class={style.Albums}>
				<h1>La brasserie en images</h1>
				<Fetcher
					query={"albums"}
					renderData={this.renderAlbums}
					loadingText="Chargement des albums en cours..." 
					errorText="Pas d'album photo ? Notre serveur est visiblement dans les choux… Si le problème persiste, contactez-nous !"
				/>
			</div>
		)
	}
}
