import { h, Component } from 'preact'
import { Link } from 'preact-router/match'

import {formatDate} from '@/utils/Utils'

import Image from '@/global/images/Image';
import style from './Album.scss';

export default class Album extends Component {

	render (props) {

		let album = props.albumData

		return (
			<Link class={style.Album} href={"/album/" + album.id} title={"Voir l'album \"" + album.title + "\""}>
				<Image class={style.AlbumImage} title={album.title} alt={album.title} src={album.cover_image ? album.cover_image : "no-thumbnail.jpg"} databaseImage cover />
				<div class={style.AlbumDesc}>
					<h4>{album.title}</h4>
					{album.date &&
						<h6>{album.date && formatDate(album.date)}</h6>
					}
				</div>
			</Link>
		);
	}
}
