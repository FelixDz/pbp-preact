import { h, Component } from 'preact'
import FsLightbox from 'fslightbox-react'
import { createPortal } from 'preact/compat'

import {formatDate} from '@/utils/Utils'
import Fetcher from '@/global/fetcher/Fetcher'

import Image from '@/global/images/Image'
import SecondaryLink from '@/global/links/secondary/SecondaryLink'

import style from './Photos.scss'

export default class Photos extends Component {

	state = {
		toggler: false,
		imageIndex: 0
	}

	setLightboxController (toggler, imageIndex) {
		this.setState({ toggler: toggler, imageIndex: imageIndex })
	}

	openLightboxOnImage (number) {
		this.setLightboxController(!this.state.toggler, number)
	}

	createLightbox (album) {
		if (typeof window !== "undefined") {
			return createPortal(
				<FsLightbox
					toggler={ this.state.toggler }
					sources={ album.photos.map(photo => { return "https://www.petitebrasseriepicarde.fr/storage/" + photo.image}) }
					customAttributes={ album.photos.map(photo => { return { alt: photo.legend !== "" ? (photo.legend) : (album.title)} }) }
					captions={ album.photos.map(photo => { return photo.legend !== "" ? photo.legend : "" }) }
					type="image"
					sourceIndex={this.state.imageIndex}
				/>,
				document.body
			);
		} else {
			return null
		}
	}

	renderAlbum = (album) => {
		return (
			<div class={style.Album}>

				<h1 class={style.AlbumTitle}>{album.title}</h1>
				{album.date &&
					<h4 class={style.AlbumSubTitle}>{ formatDate(album.date) }</h4>
				}

				<div class={style.AlbumDesc}><p>{album.desc}</p></div>

				{ this.createLightbox(album) }

				<div class={style.PhotosList}>
					{
						album.photos.map((photo, index) => {
							return (
								<div class={style.Photo} key={photo.id} onClick={() => this.openLightboxOnImage(index)}>
									<Image class={style.PhotoImage}
										title={photo.legend !== "" ? (photo.legend) : (album.title)}
										alt={photo.legend !== "" ? (photo.legend) : (album.title)}
										src={photo.image}
										small databaseImage cover flatShadow smallRounded
									 />

									{ photo.legend &&
										<div class={style.PhotoLegend}>
											{photo.legend}
										</div>
									}
								</div>
							)
						})
					}
				</div>
			</div>
		)
	}

	render(props) {

		return (
			<div class={style.Photos}>
				<Fetcher
					query={"album/" + this.props.albumId}
					renderData={this.renderAlbum}
					loadingText="Chargement des photos en cours..." 
					errorText="Eh bien ? Il semblerait que le serveur soit parti à la taverne, et ne réponde plus… Si le problème persiste, contactez-nous !"
				/>
				<p><SecondaryLink href="/photos" title="Retourner &agrave; la liste des albums photo" backward >Retourner &agrave; la liste des albums</SecondaryLink></p>
			</div>
		)
	}
}
