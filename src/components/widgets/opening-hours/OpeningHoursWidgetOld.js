import { h, Component } from 'preact'

import Primalink from '@/global/links/primalink/Primalink'
import style from './OpeningHoursWidget.scss'

export default class OpeningHoursWidget extends Component {
	constructor() {
		super()

		this.state = {
			openingHours: {}
		}
	}

	componentWillMount() {
		const openingHours = {
			summer: {
				start: {
					month: 5,
					day: 15
				},
				openings: [
					{
						day: 3,
						start: 18,
						end: 20
					},
					{
						day: 4,
						start: 18,
						end: 20
					},
					{
						day: 5,
						start: 18,
						end: 22
					},
					{
						day: 6,
						start: 14,
						end: 22
					}
				]
			},
			winter: {
				start: {
					month: 9,
					day: 15
				},
				openings: [
					{
						day: 3,
						start: 18,
						end: 20
					},
					{
						day: 4,
						start: 18,
						end: 20
					},
					{
						day: 5,
						start: 18,
						end: 20
					},
					{
						day: 6,
						start: 14,
						end: 20
					}
				]
			}
		}

		this.setState({
			openingHours: openingHours
		})
	}

	getOpeningState(currentDate) {
		let op = this.state.openingHours

		const summerStart = new Date(currentDate.getFullYear(), op.summer.start.month - 1, op.summer.start.day)
		const winterStart = new Date(currentDate.getFullYear(), op.winter.start.month - 1, op.winter.start.day)
		const weekDays = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche']

		// Get correct opening hours (summer / winter)
		let openings = currentDate >= summerStart && currentDate < winterStart ? op.summer.openings : op.winter.openings

		let currentDay = currentDate.getDay() !== 0 ? currentDate.getDay() : 7
		let currentHours = currentDate.getHours()
		let isOpen = false
		let message = []

		// Loop through opening hours of the week
		for (let openSeq of openings) {
			if (message.length !== 0) continue

			if (currentDay < openSeq.day) {
				// Open later in the week
				message = ['Ouvre ' + weekDays[openSeq.day - 1], ' de ' + openSeq.start + 'h à ' + openSeq.end + 'h']
			} else if (currentDay === openSeq.day) {
				// Open today

				if (currentHours < openSeq.start) {
					// Later today
					message = ['Ouvre ce soir', ' de ' + openSeq.start + 'h à ' + openSeq.end + 'h']
				} else if (currentHours < openSeq.end) {
					// Currently open
					isOpen = true
					message = ['Fermeture', ' à ' + openSeq.end + 'h']
				}
			}
		}

		// Not open later in this week -> display first opening next week
		if (message === '') {
			message = ['Ouvre ' + weekDays[openings[0].day - 1], ' de ' + openings[0].start + 'h à ' + openings[0].end + 'h']
		}

		return { isOpen: isOpen, message: message }
	}

	render() {
		let { isOpen, message } = this.getOpeningState(new Date())

		return (
			<section class={ [style.OpeningHoursWidget, 'Widget'].join(' ') }>
				<Primalink
					path="/"
					alt="Horaires d'ouverture"
					title="Horaires d'ouverture"
					caption={isOpen ? 'Ouvert' : 'Fermé'}
					subcaption={message}
					isPrimary={isOpen}
				/>
			</section>
		)
	}
}
