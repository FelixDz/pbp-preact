import { h, Component } from 'preact'
import { Link } from 'preact-router/match'

import Primalink from '@/global/links/primalink/Primalink'
import style from './OpeningHoursWidget.scss'

import Popup from '@/global/popup/Popup'

export default class OpeningHoursWidget extends Component {

	state = {
		openingHoursPopup: false
	}

	constructor() {
		super()
	}

	componentWillMount() {

	}

	dismissOpeningHoursPopup = () => {
		this.setState({ openingHoursPopup: false })
	}

	openOpeningHoursPopup = (event) => {
		this.setState({ openingHoursPopup: true })
	}

	// <span onClick={this.openOpeningHoursPopup.bind(this)}>Ouvrir une fen&ecirc;tre modale</span>

	render(state) {
		return (
			<section class={ [style.OpeningHoursWidget, 'Widget'].join(' ') }>
				<h3 class={style.OpeningHoursWidgetTitle}>
					Nos horaires
				</h3>

				<Primalink class={style.OpeningHoursWidgetLink}
					path=""
					alt="Horaires d'ouverture de la brasserie"
					title="Horaires d'ouverture de la brasserie"
					caption={<span class={style.OpeningHoursWidgetLinkSpan}>Samedi de 15h &agrave; 20h</span>}
					onClick={this.openOpeningHoursPopup.bind(this)}
					subcaption={["Et en semaine ", "sur rendez-vous"]}
					isPrimary={false}
				/>

				<Popup isOpen={this.state.openingHoursPopup} onDismiss={this.dismissOpeningHoursPopup} title="Ouverture de la brasserie">
					<p>
						La brasserie est ouverte les <span class="bold textPrimary">samedis de 15 &agrave; 20h</span> et en semaine <span class="bold">sur rendez-vous</span>.
					</p>

					<p>
						Des visites sont &eacute;galement possibles <Link href="/visites" alt="Se renseigner sur les visites" title="Se renseigner sur les visites" onClick={this.dismissOpeningHoursPopup}>sur r&eacute;servation</Link>.
					</p>
				</Popup>
			</section>
		)
	}
}
