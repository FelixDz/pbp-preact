import { h, Component } from 'preact'
import style from './MapWidget.scss'

export default class MapWidget extends Component {

	constructor() {

		super()

		this.state = {}
	}

	render() {

		return (
			<section class={ [style.MapWidget, 'Widget'].join(' ') }>
				<iframe width="100%" height="500px" frameBorder="0" src="https://umap.openstreetmap.fr/fr/map/points-de-vente-de-la-petite-brasserie-picarde_245741?scaleControl=false&miniMap=false&scrollWheelZoom=true&zoomControl=true&allowEdit=false&moreControl=false&searchControl=true&tilelayersControl=false&embedControl=false&datalayersControl=true&onLoadPanel=none&captionBar=false&fullscreenControl=true&locateControl=false&measureControl=false&editinosmControl=false"></iframe>
			</section>
		)
	}
}
