import { h, Component } from 'preact'
import { Link } from 'preact-router/match'

import Fetcher from '@/global/fetcher/Fetcher'
import Image from '@/global/images/Image'

import style from './AwardsWidget.scss'

export default class AwardsWidget extends Component {

	renderAwards = (awards) => {
		return (
			<div class={style.AwardsWidgetItems}>
				{	// TODO : add API route for returning only the 5 best awards (awards/best) -> change award model ?
					awards.slice(0, 5).map(award => {
						return (
							<Link class={style.AwardsWidgetLink} href={"/recompenses/" + award.id} title={award.award_title + " (" + award.year + ")"} alt={award.award_title + " (" + award.year + ")"} >
								<Image class={style.AwardsWidgetImage} title={award.award_title + " (" + award.year + ")"} alt={award.award_title + " (" + award.year + ")"} src={award.image} thumb databaseImage rounded />
							</Link>
						)
					})
				}
				<Link class={style.AwardsWidgetLink} href="/recompenses" title="Voir toutes les récompenses" alt="Voir toutes les récompenses">
					<div class={style.AwardsWidgetLastLink}>
						<div>
							...&nbsp;et&nbsp;bien<br />
							d'autres
						</div>
					</div>
				</Link>
			</div>
		)
	}

	render() {

		return (
			<section class={[style.AwardsWidget, "Widget"].join(" ")}>
				<h3 class={style.AwardsWidgetTitle}>
					<Link href="/recompenses" title="" alt="Prix & récompenses">
						Prix & r&eacute;compenses
					</Link>
				</h3>

				<Fetcher
					query="awards"
					renderData={this.renderAwards}
					loadingText=" " 
					errorText="Une erreur est survenue lors du chargement des prix et récompenses."
				/>
			</section>
		)
	}
}
