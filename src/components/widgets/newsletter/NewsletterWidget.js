import { h, Component } from 'preact'
import { Link } from 'preact-router/match'

import style from './NewsletterWidget.scss'

import Primalink from '@/global/links/primalink/Primalink'
import Popup from '@/global/popup/Popup'
import Spacer from '@/global/spacer/Spacer'

export default class NewsletterWidget extends Component {

	state = {
		newsletterPopup: false
	}

	constructor() {
		super()
	}

	componentWillMount() {

	}

	dismissNewsletterPopup = () => {
		this.setState({ newsletterPopup: false })
	}

	openNewsletterPopup = (event) => {
		event.preventDefault()
		this.setState({ newsletterPopup: true })
	}

	handleSubmit = (event) => {
		this.dismissNewsletterPopup()
		console.log("Form submitted !")
		// TODO actually send email, but howwwwdy how ?
	}

	render() {
		return (
			<section class={[style.NewsletterWidget, "Widget"].join(" ")}>
				<Primalink
					path=""
					alt="Inscrivez-vous à notre newsletter !"
					title="Inscrivez-vous à notre newsletter !"
					caption="Suivez nos actualités"
					onClick={this.openNewsletterPopup.bind(this)}
					isPrimary
				/>
				<span class={style.NewsletterWidgetSubcaption}>
					environ un mail par mois
				</span>

				<Popup isOpen={this.state.newsletterPopup} onDismiss={this.dismissNewsletterPopup} title="Inscription à la newsletter">

					<form class={[style.NewsletterWidgetSubForm, "plume"].join(" ")}>

						<div class="pm-field" data-pm-success="Adresse mail valide">
							<label for="input-email" class="pm-required">Votre adresse mail</label>
							<input type="email" class="pm-error" placeholder="" name="input-email" />
						</div>

						<Spacer />

						<p>
							Informations facultatives (mais appréciées !) :
						</p>

						<div class="twoInputs">
							<div class="pm-field">
								<label for="firstname">Prénom</label>
								<input type="text" placeholder="" name="firstname" />
							</div>
							<div class="separator"></div>
							<div class="pm-field">
								<label for="lastname">Nom</label>
								<input type="text" placeholder="" name="lastname" />
							</div>
							<div class="separator"></div>
							<div class="pm-field">
								<label for="zipcode">Code postal</label>
								<input type="text" placeholder="" name="zipcode" />
							</div>							
						</div>

						<p class="pm-text-center">
							<Primalink
								path=""
								onClick={this.handleSubmit}
								alt="S'inscrire"
								title="S'inscrire"
								caption="S'inscrire"
								isPrimary
							/>
						</p>

						<Spacer small />
					</form>
				</Popup>
			</section>
		)
	}
}
