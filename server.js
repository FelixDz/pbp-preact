if (typeof(PhusionPassenger) !== 'undefined') {
    PhusionPassenger.configure({ autoInstall: false });
}

const fs = require('fs')
const { h } = require('preact')
const express = require('express')
const { join, basename } = require('path')
const compression = require('compression')()
const render = require('preact-render-to-string')
const bundle = require('./build/ssr-build/ssr-bundle')
const helmet = require('preact-helmet')

const App = bundle.default
const PORT = (typeof(PhusionPassenger) !== 'undefined') ? 'passenger' : process.env.PORT || 3000
// TODO: improve this?
const REGEX_CONTENT = /<div id="app"[^>]*>.*?(?=<script)/i
// TODO update replacement  cf https://github.com/download/preact-helmet
const REGEX_METAS = /<title>.*?<\/title>/i

const assets = join(__dirname, 'build')
const template = fs.readFileSync('./build/index.html', 'utf8')
const favicon = require('serve-favicon')(join(assets, 'favicon.ico'))

function setHeaders(res, file) {
	let cache = basename(file) === 'sw.js' ? 'private,no-cache' : 'public,max-age=31536000,immutable'
	res.setHeader('Cache-Control', cache) // disable service worker cache
}

express()
	.use(favicon)
	.use(compression)
	.use(express.static(assets, { setHeaders }))
	.get('*', (req, res) => {
		let url = req.url
		let body = render(h(App, { url }))
		let helmetHead = helmet.rewind()
		let head = `${helmetHead.title.toString()}${helmetHead.meta.toString()}${helmetHead.link.toString()}`
		res.send(template
			.replace(REGEX_CONTENT, body)
			.replace(REGEX_METAS, head))
	})
	.listen(PORT, err => {
		if (err) {
			res.sendStatus(404)
			throw err
		}
		console.log(`> SSR-server running on localhost:${PORT}`)
	})
