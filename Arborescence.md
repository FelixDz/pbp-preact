### Arborescence du site

#### Actuelle

- Accueil
	+ Nos bières
		- Découvrir nos bières
		- Trouver nos bières
	+ Brasserie
		- L'histoire du lieu
		- Venir visiter
	+ Animations
	+ Taverne
	+ Repas
	+ Contact


#### Proposée

- Accueil : présentation et dernières nouvelles, avis
	+ Nos bières (ici : volonté, bio)
		- Découvrir notre gamme (rappel bio + élaboration)
        - Nos produits récompensés (prix, médailles)
		- Trouver nos bières (carte)
	+ La brasserie
		- Notre histoire
        - Boire (et manger) sur place (bar, rappel horaires, repas)
		- Venir visiter (+ lien vers contact)
	+ Évènements
		- À la brasserie
		- Sur les marchés, fêtes…
	+ Médias
		- Albums photos
		- Vidéos (vidéos explicatives / reportages & interviews)
		- Humour brassicole (citations)
        - Il·elles ont laissé leur avis (livre d'or avec FB / TripAdvisor)
	+ Contact (entrer en contact ou travailler avec nous)
        - Nous contacter (téléphone, mail, adresse & carte)
        - Devenir partenaires professionnels (coin des pros) 

Autres :

- Partenaires, sites amis => footer
- Livre d'or => supprimé, avis sur le site
- Presse => supprimé, ou footer
- Coin des pros => supprimé, section sur la page de contact
- CGV => footer
- Calendrier => supprimé au profit des dernières nouvelles (accueil) et animations (pages dédiées)
- Horaires => widget sur le côté
